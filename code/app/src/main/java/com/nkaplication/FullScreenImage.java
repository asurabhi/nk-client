package com.nkaplication;

import com.nkaplication.adp.FullScreenImageAdapter;
import com.nkaplication.model.AdItem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class FullScreenImage extends BaseActivity {

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.full_screen_image);
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		
		AdItem item = NKApplication.getInstance().selectedItem;
		
		int position = getIntent().getIntExtra("position", 0);
		
		if(item != null){
		adapter = new FullScreenImageAdapter(FullScreenImage.this,item.images);
        viewPager.setAdapter(adapter);
        // displaying selected image first
		viewPager.setCurrentItem(position);
		}
	}
}
