package com.nkaplication;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.nkaplication.adp.MenuListAdapter;
import com.nkaplication.model.MenuDrawerItem;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.Constants;
import com.nkaplication.util.CustomDialog;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Util;

public class BaseActivity extends Activity implements DrawerListener,
        ConnectionCallbacks, OnConnectionFailedListener, Constants,
        ResultCallback<People.LoadPeopleResult> {
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<MenuDrawerItem> navDrawerItems;
    ImageView menuListButton;
    Handler h = new Handler();
    ProgressDialog p = null;
    private DrawerLayout drawerLayout;
    private ListView list;
    GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    public boolean mSignInClicked;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;
    Context ctx = null;
    public boolean checkLoggedIn = false;
    public boolean checkForSendButton = false;
    private String loadingMessage = "";

    /**
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE).build();

    }

    /**
     * Method for recreate google+ login api client.........
     */
    public void setGoogleObject() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE).build();
    }

    /**
     * Method for progress bar loading
     */

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_LOADING) {
            p = new ProgressDialog(this);// LoadingBar(this, "Loading...");
            p.setTitle(loadingMessage);
            p.setCancelable(false);
            return p;

        } else {
        }
        return super.onCreateDialog(id);
    }

    /**
     * Method of close dialog
     */
    public void closeDialog(int id) {
        if (p != null && p.isShowing())
            this.dismissDialog(id);
    }

    /**
     * Method for onClick back for all activities
     */
    public void goBack(View v) {
        finish();
    }

    /**
     * Method for show dialog
     */
    public void startBusy() {
        startBusy("Please Wait...");
    }

    /**
     * Method for show dialog
     */
    public void startBusy(String msg) {
        this.loadingMessage = msg;
        showDialog(DIALOG_LOADING);
    }

    /**
     * Method for stop Dialog
     */
    public void stopBusy() {
        post(new Runnable() {

            @Override
            public void run() {
                closeDialog(DIALOG_LOADING);

            }
        });
    }

    public void post(Runnable r) {
        h.post(r);
    }

    /* Action on ActionBar */
    public void initActionBar(final DrawerLayout drawerLayout,
                              final ListView list) {
        this.drawerLayout = drawerLayout;
        this.list = list;
        /*
		 * final ActionBar actionBar = getActionBar();//
		 * .setCustomView(R.layout.action_bar_custome_layout);
		 * actionBar.setDisplayShowHomeEnabled(false);
		 * actionBar.setDisplayShowTitleEnabled(false);
		 * actionBar.setDisplayShowCustomEnabled(true);
		 * actionBar.setCustomView(R.layout.action_bar_custome_layout);
		 */

        // menuListButton = (ImageView) findViewById(R.id.menuListButton);
        menuListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(list))
                    drawerLayout.closeDrawer(list);
                else {
                    MyPrefs.getInstance().loadPrefs(BaseActivity.this);
                    drawerLayout.openDrawer(list);
                }

            }
        });

        setCategoryList();
    }

    /**
     * Method for set slider list.........
     */
    private void setCategoryList() {
        // TODO Auto-generated method stub
        setDummyData();
        MenuListAdapter adapter = new MenuListAdapter(this, navDrawerItems);
        if (MyPrefs.getInstance().email.equalsIgnoreCase("")) {
            navDrawerItems.remove(navDrawerItems.size() - 1);
        }
        list.setAdapter(adapter);
    }

    /**
     * Set Dummy data for menu slider
     */
    private void setDummyData() {
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        navDrawerItems = new ArrayList<MenuDrawerItem>();

        for (int i = 0; i < navMenuTitles.length; i++) {
            navDrawerItems.add(new MenuDrawerItem(navMenuTitles[i],
                    navMenuIcons.getResourceId(i, -1)));
        }

    }

	/*
	 * public void initActionBarHeader(String title) {
	 * 
	 * final ActionBar actionBar = getActionBar();//
	 * .setCustomView(R.layout.action_bar_custome_layout);
	 * actionBar.setDisplayShowHomeEnabled(false);
	 * actionBar.setDisplayShowTitleEnabled(false);
	 * actionBar.setDisplayShowCustomEnabled(true);
	 * actionBar.setCustomView(R.layout.header); TextView headerTitle =
	 * (TextView) findViewById(R.id.headerTitle); headerTitle.setText(title);
	 * 
	 * }
	 */

    @Override
    public void onDrawerClosed(View arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDrawerOpened(View arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDrawerSlide(View arg0, float arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDrawerStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    /**
     * perform action click on any item of menu slider
     */
    public void onClickButton(int image) {
        drawerLayout.closeDrawer(list);
        switch (image) {
            case 1:
                startActivity(new Intent(BaseActivity.this, MyAccount.class));
                break;
            case 2: {
                Util.reload = false;
                Intent intent = new Intent(BaseActivity.this,
                        MyAdsAndShortList.class);
                intent.putExtra("comeFrom",
                        getResources().getString(R.string.my_ads));
                startActivity(intent);
                break;
            }
            case 3: {
                Util.reload = false;
                Intent intent = new Intent(BaseActivity.this,
                        MyAdsAndShortList.class);
                intent.putExtra("comeFrom",
                        getResources().getString(R.string.my_shortlist));
                startActivity(intent);
                break;
            }
            case 4: {
                startActivity(new Intent(BaseActivity.this, AboutUs.class));
                break;
            }
            case 5: {
                MyPrefs.getInstance().email = "";
                MyPrefs.getInstance().userFirstName = "";
                MyPrefs.getInstance().userLastName = "";
                MyPrefs.getInstance().savePrefs(BaseActivity.this);
                finish();
                Intent intent = new Intent(BaseActivity.this, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("extra", true);
                startActivity(intent);
                break;
            }
            default:

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;

            if (mGoogleApiClient != null && !mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }

            if (mGoogleApiClient == null) {
                setGoogleObject();
                mGoogleApiClient.connect();
            }


        }
    }

    /*
     * Method call when google+ login successfully connected..........
     * (non-Javadoc)
     *
     * @see
     * com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
     * #onConnected(android.os.Bundle)
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;

        System.out.println();
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
                this);
        getProfileInformation();

    }

    /**
     * Method for getting user information after successful login from
     * google+....
     */
    public void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG)
                        .show();
                Person currentUser = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String fullName = currentUser.getDisplayName();
                String imageUrl = currentUser.getImage().getUrl();
                String googleId = currentUser.getId();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                saveUserInfoInPrefs(fullName, imageUrl, googleId, email);

                // System.out.print("");
            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            stopBusy();
        }
    }

    /**
     * Method for storing detail in preference from google+......
     *
     * @param fullName
     * @param imageUrl
     * @param googleId
     * @param email
     */
    private void saveUserInfoInPrefs(String fullName, String imageUrl,
                                     String googleId, String email) {
        String firstName, lastName;
        if (fullName.contains(" ")) {
            String[] fullNameArr = fullName.split(" ");
            firstName = fullNameArr[0];
            lastName = fullNameArr[1];
        } else {
            firstName = fullName;
            lastName = "";
        }

        MyPrefs.getInstance().userFirstName = firstName;
        MyPrefs.getInstance().userLastName = lastName;
        MyPrefs.getInstance().email = email;
        MyPrefs.getInstance().googleId = googleId;
        MyPrefs.getInstance().imageUrl = imageUrl;
        MyPrefs.getInstance().savePrefs(getApplicationContext());
        if (ctx != null && ctx instanceof MyAccount) {
            ((MyAccount) ctx).setData();
        } else if (ctx != null && ctx instanceof PostAd) {
            ((PostAd) ctx).setEmailOnId();
            // ((PostAd) ctx).postDatalist();
        } else if (ctx != null && ctx instanceof MyAdsAndShortList) {
            ((MyAdsAndShortList) ctx).fetchFilesByIdAndLoad();
        } else if (ctx != null && ctx instanceof ItemDetail) {
            ((ItemDetail) ctx).setHeaderWithSlider();

            showDialogForSucessFulSignInGoogle();
        } else if (ctx != null && ctx instanceof SubCategories) {
            ((SubCategories) ctx).setHeaderWithSlider();

            showDialogForSucessFulSignInGoogle();
        }

    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();

    }

    /*
     * Method for call when google+ login connection failed..........
     * (non-Javadoc)
     *
     * @see
     * com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
     * #onConnectionFailed(com.google.android.gms.common.ConnectionResult)
     */
    @Override
    public void onConnectionFailed(final ConnectionResult result) {
        stopBusy();

        // new CustomDialog(BaseActivity.this, "Error",
        // "Error while signing in with Google+. Please try again", new
        // String[]{"OK"}, null).show();
        if (!result.hasResolution()) {
			/*
			 * GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(),
			 * this, 0).show();
			 */
            new CustomDialog(BaseActivity.this, "Error",
                    "Error while signing in with Google+. Please try again",
                    new String[]{"OK"}, new DialogButtonListener() {

                @Override
                public void onButtonClicked(String text) {
                    if (ctx != null
                            && !((ctx instanceof SubCategories) || (ctx instanceof ItemDetail))) {
                        finish();
                    }

                }
            }).show();

            return;
        } else {
            if (!mIntentInProgress) {
                // Store the ConnectionResult for later usage
                mConnectionResult = result;

                if (checkLoggedIn) {
                    return;
                }

                checkLoggedIn = true;
                if (mSignInClicked) {
                    // The user has already clicked 'sign-in' so we attempt to

                    // resolve all
                    // errors until the user is signed in, or they cancel.
                    resolveSignInError();
                } else {
                    if (ctx != null && ctx instanceof MyAccount) {
                        ((MyAccount) ctx).setAccountGoogleData();
                    } else if (ctx != null && ctx instanceof MyAdsAndShortList) {
                        ((MyAdsAndShortList) ctx).setGoogleData();
                    } else if (ctx != null && ctx instanceof PostAd) {
                        ((PostAd) ctx).setPostAdGoogleData();
                    } else if (ctx != null && ctx instanceof SubCategories) {
                        ((SubCategories) ctx).setHeaderWithSlider();
                        ((SubCategories) ctx).setSubCatAdGoogleData();
                    }

                    if (ctx != null && ctx instanceof ItemDetail) {
                        ((ItemDetail) ctx).setHeaderWithSlider();
                        ((ItemDetail) ctx).setItemDetailAdGoogleData();

                    }

                }
            }
        }

    }

    /**
     * Method for resolving sign in error in google+.........
     */
    public void resolveSignInError() {

        if (mConnectionResult == null) {
            Toast.makeText(this, " Connection Failed ! Please try again.",
                    Toast.LENGTH_SHORT).show();
            stopBusy();
            return;
        }

        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (Exception e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        // if(Util.isNetworkAvailable(getApplicationContext()))
        System.out.println();

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        } else {
            mGoogleApiClient = null;

            stopBusy();
        }

    }

    @Override
    public void onResult(LoadPeopleResult result) {
        // TODO Auto-generated method stub
        System.out.println();

    }

    /**
     * Method for starting home activity.......
     *
     * @param v
     */
    public void performActionGoToHomeScreen(View v) {
        Intent intent = new Intent(BaseActivity.this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Method for dialog showing network problem............
     */
    public void showNetworkError() {
        post(new Runnable() {

            @Override
            public void run() {
                new CustomDialog(BaseActivity.this, getResources().getString(
                        R.string.connectivity_issue_text), getResources()
                        .getString(R.string.poor_network_text),
                        new String[]{getResources().getString(
                                R.string.ok_text)},
                        new DialogButtonListener() {
                            @Override
                            public void onButtonClicked(String text) {
                                Intent intent = new Intent(BaseActivity.this,
                                        Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }).show();

            }
        });

    }

    /**
     * Method for showing dialog for confirmation google+ sign in.......
     */
    public void showDialogForSignInGoogle(DialogButtonListener dl) {

        new CustomDialog(BaseActivity.this, "Google+ Sign In",
                "Are you sure you want to sign with google+ ?", new String[]{
                "No", "Yes"}, dl).show();

		/*
		 * final Dialog dlg=new Dialog(BaseActivity.this,
		 * R.style.FullHeightDialog);
		 * dlg.setContentView(R.layout.google_sign_in_confirm_dialog);
		 * dlg.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
		 * ViewGroup.LayoutParams.WRAP_CONTENT);
		 * dlg.getWindow().setGravity(Gravity.CENTER);
		 * 
		 * 
		 * Button noButton = (Button) dlg .findViewById(R.id.noButton); Button
		 * yesButton = (Button) dlg .findViewById(R.id.yesButton);
		 * 
		 * 
		 * noButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { if(ctx != null && !((ctx
		 * instanceof SubCategories) || (ctx instanceof ItemDetail))){ finish();
		 * } dlg.dismiss(); } });
		 * 
		 * yesButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dlg.dismiss(); if
		 * (mGoogleApiClient != null){ startBusy(); mGoogleApiClient.connect();
		 * } } });
		 * 
		 * dlg.show();
		 */

    }

    public void showDialogForSucessFulSignInGoogle() {
        final Dialog dlg = new Dialog(BaseActivity.this,
                R.style.FullHeightDialog);
        dlg.setContentView(R.layout.google_sign_in_confirm_dialog);
        dlg.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dlg.getWindow().setGravity(Gravity.CENTER);

        Button noButton = (Button) dlg.findViewById(R.id.noButton);
        Button yesButton = (Button) dlg.findViewById(R.id.yesButton);
        TextView messageText = (TextView) dlg.findViewById(R.id.messageText);

        noButton.setVisibility(View.GONE);
        yesButton.setText("OK");
        messageText.setText("You are successfully Sign In with Google+");

        yesButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
                if (ctx != null && ctx instanceof ItemDetail) {
                    ((ItemDetail) ctx).setHeaderWithSlider();
                    ((ItemDetail) ctx).performActionIsLiked(null);
                } else if (ctx != null && ctx instanceof SubCategories) {
                    ((SubCategories) ctx).setHeaderWithSlider();
                    ((SubCategories) ctx).checkLikeAfterGoogleLogin();
                }

            }
        });

        dlg.show();

    }


    public void showDialogForSucessFulSubCatSignInGoogle() {
        final Dialog dlg = new Dialog(BaseActivity.this,
                R.style.FullHeightDialog);
        dlg.setContentView(R.layout.google_sign_in_confirm_dialog);
        dlg.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dlg.getWindow().setGravity(Gravity.CENTER);

        Button noButton = (Button) dlg.findViewById(R.id.noButton);
        Button yesButton = (Button) dlg.findViewById(R.id.yesButton);
        TextView messageText = (TextView) dlg.findViewById(R.id.messageText);

        noButton.setVisibility(View.GONE);
        yesButton.setText("OK");
        messageText.setText("You are successfully Sign In with Google+");

        yesButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();


            }
        });

        dlg.show();

    }


}
