package com.nkaplication.net;

public interface DataListener {
	
	public void onDataResponse(boolean isError, Response r, int rid, Object data);

}
