package com.nkaplication;

import java.util.ArrayList;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nkaplication.adp.CategoryAdapter;
import com.nkaplication.adp.SpinnerAdapter;
import com.nkaplication.model.MenuDrawerItem;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Util;

/**
 * @author neelam goyal
 * 
 *         this Activity will be open from Home Activity
 * 
 */

public class Category extends BaseActivity implements Constants {

	private DrawerLayout drawer_layout;
	private ListView electronicMobileList;
	private ListView electronicAdsList;

	private Spinner electronicSp;
	private String[] electronicMenuTitles;
	private TypedArray electronicMenuIcons;

	private ArrayList<MenuDrawerItem> electronicCategoryItems;
	private String[] categoryArray;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.electronics);
		electronicAdsList = (ListView) findViewById(R.id.electronicAdsList);
		electronicMobileList = (ListView) findViewById(R.id.menuList);
		drawer_layout = (DrawerLayout) findViewById(R.id.electronic_drawer_layout);
		electronicSp = (Spinner) findViewById(R.id.electronicSpinnerId);

		menuListButton = (ImageView) findViewById(R.id.electricMenuIcon);
	
		drawer_layout.setDrawerListener(this);

		setDummyData();
		CategoryAdapter adapter = new CategoryAdapter(this,
				electronicCategoryItems);
		electronicAdsList.setAdapter(adapter);

		electronicAdsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				if (position == 0) {
					NKApplication.getInstance().selectedSubCategory = KEY_MOBILES;
					Intent intent = new Intent(Category.this,
							SubCategories.class);
					startActivity(intent);
				} else if (position == 1) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
				} else if (position == 2) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		categoryArray = getResources().getStringArray(R.array.homeItemCategory);

		for (int i = 0; i < categoryArray.length; i++) {
			if (categoryArray[i]
					.equalsIgnoreCase(NKApplication.getInstance().selectedCategory)) {
				String temp = categoryArray[0];
				categoryArray[0] = categoryArray[i];
				categoryArray[i] = temp;

			}

		}

		electronicSp.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View view,
					int arg2, long arg3) {

				String selectedCategoryItem = electronicSp.getSelectedItem()
						.toString();

				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_AUTO_MOBILES)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
					setSpinnerAdapterBySelectedItem();
					return;

				}
				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_REAL_ESTATE)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
					setSpinnerAdapterBySelectedItem();
					return;

				}
				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_HOME_AND_FURNITURE)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();

					setSpinnerAdapterBySelectedItem();

					return;

				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		setSpinnerAdapterBySelectedItem();
	}

	/**
	 * Method for set spinner value for selected item.........
	 */
	protected void setSpinnerAdapterBySelectedItem() {
		SpinnerAdapter subcategoryAdp = new SpinnerAdapter(Category.this,
				categoryArray, R.layout.spinner_text_view);

		subcategoryAdp
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		electronicSp.setAdapter(subcategoryAdp);
		electronicSp.setEnabled(true);
		electronicSp.setClickable(true);
	}

	/**
	 * method of menu slider
	 */
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setHeaderWithSlider();
	}

	/**
	 * Method for setting menu list slider with data........
	 */
	private void setHeaderWithSlider() {

		initActionBar(drawer_layout, electronicMobileList);
		drawer_layout.setDrawerListener(this);
	}

	/**
	 * Performe action click on any item of MenuList slider
	 */

	/** Start an activity of MobileElectronics */

	public void perfomreMobileElectronics(View v) {
		NKApplication.getInstance().selectedSubCategory = KEY_MOBILES;
		Intent intent = new Intent(Category.this, SubCategories.class);
		startActivity(intent);
	}

	/**
	 * Method for set dummy data for electronic menu tiles list..........
	 */
	private void setDummyData() {
		electronicMenuTitles = getResources().getStringArray(
				R.array.electronics_item);

		// nav drawer icons from resources
		electronicMenuIcons = getResources().obtainTypedArray(
				R.array.electronic_icons);

		electronicCategoryItems = new ArrayList<MenuDrawerItem>();

		for (int i = 0; i < electronicMenuIcons.length(); i++) {
			electronicCategoryItems.add(new MenuDrawerItem(electronicMenuTitles[i+1],
					electronicMenuIcons.getResourceId(i, -1)));
		}

	}

}