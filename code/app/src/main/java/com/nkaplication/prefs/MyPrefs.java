package com.nkaplication.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPrefs {

	static MyPrefs instance = new MyPrefs();
	final String PREF_NAME = "myPrefs";

	final String PREF_USER_ID = "userId";
	final String PREF_USER_FIRST_NAME = "userFirstName";
	final String PREF_USER_LAST_NAME = "userLastName";
	final String PREF_EMAIL = "email";
	final String PREF_IMAGE_URL = "imageUrl";
	final String PREF_GOOGLE_ID = "googleId";
	final String PREFS_MOBILE_NUMBER = "mobileNumber";

	public String userId;
	public String userFirstName;
	public String userLastName;
	public String email = "";
	public String imageUrl;
	public String googleId;

	SharedPreferences.Editor e;
	public String mobileNumber;

	public static MyPrefs getInstance() {
		return instance;
	}

	/**
	 * Get data from preference and stored in Application variable.
	 * 
	 * @param ctx
	 */
	public void loadPrefs(Context ctx) {
		SharedPreferences p = ctx.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);
		userId = p.getString(PREF_USER_ID, "");
		userFirstName = p.getString(PREF_USER_FIRST_NAME, "");
		userLastName = p.getString(PREF_USER_LAST_NAME, "");
		email = p.getString(PREF_EMAIL, "");
		imageUrl = p.getString(PREF_IMAGE_URL, "");
		googleId = p.getString(PREF_GOOGLE_ID, "");
		mobileNumber = p.getString(PREFS_MOBILE_NUMBER, "");

	}

	/**
	 * Save data in shared preference.
	 * 
	 * @param ctx
	 * @return
	 */
	public boolean savePrefs(Context ctx) {
		e = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
		e.putString(PREF_USER_ID, userId);
		e.putString(PREF_USER_FIRST_NAME, userFirstName);
		e.putString(PREF_USER_LAST_NAME, userLastName);
		e.putString(PREF_EMAIL, email);
		e.putString(PREF_IMAGE_URL, imageUrl);
		e.putString(PREF_GOOGLE_ID, googleId);
		e.putString(PREFS_MOBILE_NUMBER, mobileNumber);

		return e.commit();

	}

	public boolean deletePrefs(Context ctx) {
		e.clear();
		return e.commit();
	}

}
