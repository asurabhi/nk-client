package com.nkaplication;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONObject;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nkaplication.adp.SpinnerAdapter;
import com.nkaplication.adp.SubCategoriesAdapter;
import com.nkaplication.db.NKDB;
import com.nkaplication.fragment.FilterFragment;
import com.nkaplication.fragment.SortFragment;
import com.nkaplication.model.AdItem;
import com.nkaplication.model.BaseModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.Constants;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Logger;
import com.nkaplication.util.Util;

/**
 * @author Brijesh dutt
 * 
 */
public class SubCategories extends BaseActivity implements DataListener,
		Constants {

	private ArrayList<AdItem> electricMobilesItems = new ArrayList<AdItem>();
	private ListView electricMobileList;
	private SubCategoriesAdapter adapter;
	private ListView lv;
	private DrawerLayout drawer_layout;
	private LinearLayout sortPopupWindow, electricMobileSortLayout,
			sortLinearLayout, headerLinearLayout;
	private ArrayList<String> allAds = new ArrayList<String>();
	private String sortType;
	// boolean checkBySortFilter;
	private String locationFilter = "", rangeFilter = "0 to 50000",
			adTypeFilter = "sell", title;
	private Spinner mobileSp;
	private SpinnerAdapter subcategoryAdp;
	private ArrayList<AdItem> listAds = new ArrayList<AdItem>();

	private LinearLayout mobileListCompleteLayout;
	private NKDB db;
	 AdItem checkAdItem = new AdItem();
	 int checkPosition;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sub_categories);
		ctx = this;
		mobileListCompleteLayout = (LinearLayout) findViewById(R.id.mobileListCompleteLayout);
		electricMobileList = (ListView) findViewById(R.id.electricMobileList);
		menuListButton = (ImageView) findViewById(R.id.electricMenuIcon);
		sortLinearLayout = (LinearLayout) findViewById(R.id.sortLinearLayout);
		headerLinearLayout = (LinearLayout) findViewById(R.id.headerLinearLayout);
		lv = (ListView) findViewById(R.id.menuList);
		drawer_layout = (DrawerLayout) findViewById(R.id.mobile_drawer_layout);
		sortPopupWindow = (LinearLayout) findViewById(R.id.sortPopupWindow);
		electricMobileSortLayout = (LinearLayout) findViewById(R.id.electricMobileSortLayout);
		// initActionBar(drawer_layout, lv);
		mobileSp = (Spinner) findViewById(R.id.mobileSpinnerId);
		sortType = getResources().getString(R.string.most_recent);
		String[] categoryArray = getResources().getStringArray(
				R.array.electronics_item_without_select_text);

		for (int i = 0; i < categoryArray.length; i++) {
			if (categoryArray[i]
					.equalsIgnoreCase(NKApplication.getInstance().selectedSubCategory)) {
				String temp = categoryArray[0];
				categoryArray[0] = categoryArray[i];
				categoryArray[i] = temp;

			}

		}

		subcategoryAdp = new SpinnerAdapter(SubCategories.this, categoryArray,
				R.layout.spinner_text_view);

		mobileSp.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View view,
					int arg2, long arg3) {

				String selectedCategoryItem = mobileSp.getSelectedItem()
						.toString();

				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_TABLETS)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
					setSpinnerAdapterBySelectedItem();
					return;

				}
				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_HOME_APPLIANCES)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
					setSpinnerAdapterBySelectedItem();
					return;

				}
				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_ANY_OTHER)) {

					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.not_implemented_yet),
							Toast.LENGTH_SHORT).show();
					setSpinnerAdapterBySelectedItem();
					return;

				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				// subCategorySpinner.setEnabled(false);
			}
		});

		setSpinnerAdapterBySelectedItem();
		/*
		 * load MobilesListItem model's load method
		 */

		 db = new NKDB(SubCategories.this);
		allAds = db.getAdsFilesByCatSubCat(
				NKApplication.getInstance().selectedCategory.toLowerCase(),
				NKApplication.getInstance().selectedSubCategory.toLowerCase());
		AdItem.load(this, this);
	

	}

	/**
	 * Method for set spinner data of adapter by selected item 
	 */
	private void setSpinnerAdapterBySelectedItem() {
		subcategoryAdp
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mobileSp.setAdapter(subcategoryAdp);
		mobileSp.setEnabled(true);
		mobileSp.setClickable(true);
	}

	/**
	 * method of menu slider
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setHeaderWithSlider();
	}

	
	
	
	
	public void setHeaderWithSlider() {

		initActionBar(drawer_layout, lv);
		drawer_layout.setDrawerListener(this);
	}

	/**
	 * Set data in mobile list adapter
	 * 
	 * @param listAds
	 */
	private void setDataIntoListView(ArrayList<AdItem> list) {
		this.listAds = list;
		adapter = new SubCategoriesAdapter(this, listAds);
		electricMobileList.setAdapter(adapter);
		electricMobileList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				NKApplication.getInstance().selectedItem = listAds
						.get(position);
				
				String path = db.getPathByAdId(NKApplication.getInstance().selectedItem.adItemId);
				
				Logger.logFile = new File(path);
				Intent intent = new Intent(SubCategories.this, ItemDetail.class);
				startActivityForResult(intent, 1);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (Util.reload) {
				Util.reload = false;
				allAds.clear();
				NKDB db = new NKDB(SubCategories.this);
				allAds = db.getAdsFilesByCatSubCat(
						NKApplication.getInstance().selectedCategory
								.toLowerCase(),
						NKApplication.getInstance().selectedSubCategory
								.toLowerCase());
				AdItem.load(this, this);
			}
		}
	}

	@Override
	public void onDataResponse(boolean isError, final Response r,
			final int rid, Object data) {
		h.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (rid == BaseModel.REQUEST_GET_ADS_LIST) {
						electricMobilesItems.clear();
						for (int i = 0; i < allAds.size(); i++) {

							String data = Logger.readLogFile(
									SubCategories.this, allAds.get(i));
							JSONObject obj = new JSONObject(data);
							AdItem allItem = new AdItem();
							allItem.populateFromJson(obj);
							electricMobilesItems.add(allItem);

						}

						Collections.sort(electricMobilesItems,
								mostRecentComparator);
						setDataIntoListView(electricMobilesItems);
					}
					
					if(rid == BaseModel.REQUEST_UPDATE__AD)
					{
						stopBusy();
						setDataIntoListView(listAds);
						//adapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/* Perform on click sortIcon button */
	public void performSortAction(View v) {

		if (sortLinearLayout.getVisibility() == View.GONE) {
			sortPopupWindow.removeAllViews();
			showSortDialog(v);
		} else {
			sortLinearLayout.setVisibility(View.GONE);
			for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
				headerLinearLayout.getChildAt(i).setEnabled(true);
			}
			electricMobileList.setEnabled(true);
			
			
		}

	}

	/**
	 * Method click on filter action........
	 * @param v
	 */
	public void performFilterAction(View v) {

		if (sortLinearLayout.getVisibility() == View.GONE) {
			sortPopupWindow.removeAllViews();
			showFilterDialog(v);
		} else {
			sortLinearLayout.setVisibility(View.GONE);
			for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
				headerLinearLayout.getChildAt(i).setEnabled(true);
			}
			electricMobileList.setEnabled(true);
		}
	}

	/**
	 * Method click on sort action...........
	 * @param v
	 */
	private void showSortDialog(View v) {

		for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
			headerLinearLayout.getChildAt(i).setEnabled(false);
		}
		electricMobileList.setEnabled(false);
		
		
		int headerHeight = headerLinearLayout.getMeasuredHeight();
		int buttonHeight = v.getMeasuredHeight();
		int topmargin = (int) Util.convertDpToPixel(30, SubCategories.this);
		int total = headerHeight + buttonHeight + topmargin;
		sortLinearLayout.setVisibility(View.VISIBLE);
		LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) sortPopupWindow
				.getLayoutParams();
		param.setMargins(topmargin, total, 0, 0);
		param.gravity = Gravity.LEFT;
		sortPopupWindow.setLayoutParams(param);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		SortFragment sortFrag = new SortFragment();
		Bundle args = new Bundle();
		args.putString("sortType", sortType);
		sortFrag.setArguments(args);
		fragmentTransaction.add(R.id.sortPopupWindow, sortFrag);
		fragmentTransaction.commit();

	}

	/**
	 *  Method to open filter dialog action...........
	 * @param v
	 */
	private void showFilterDialog(View v) {
		for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
			headerLinearLayout.getChildAt(i).setEnabled(false);
		}
		electricMobileList.setEnabled(false);
		int headerHeight = headerLinearLayout.getMeasuredHeight();
		int buttonHeight = v.getMeasuredHeight();
		int topmargin = (int) Util.convertDpToPixel(30, SubCategories.this);
		int total = headerHeight + buttonHeight + topmargin;
		sortLinearLayout.setVisibility(View.VISIBLE);
		LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) sortPopupWindow
				.getLayoutParams();
		param.setMargins(0, total, topmargin, 0);
		param.gravity = Gravity.RIGHT;
		sortPopupWindow.setLayoutParams(param);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		FilterFragment filterFragment = new FilterFragment();
		Bundle args = new Bundle();
		args.putString("location", locationFilter);
		args.putString("range", rangeFilter);
		args.putString("adType", adTypeFilter);
		args.putString("title", title);
		filterFragment.setArguments(args);
		fragmentTransaction.add(R.id.sortPopupWindow, filterFragment);
		fragmentTransaction.commit();

	}

	/**
	 * Method for reset sort dialog action.......
	 * @param sortedByValue
	 */
	public void resetSortLayout(String sortedByValue) {
		sortType = sortedByValue;
		sortLinearLayout.setVisibility(View.GONE);
		for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
			headerLinearLayout.getChildAt(i).setEnabled(true);
		}
		electricMobileList.setEnabled(true);
		
		sortListByParameter(sortType);
	}

	/**
	 * Method for sort list by different parameters........
	 * @param sortType
	 */
	private void sortListByParameter(String sortType) {

		if (sortType.equalsIgnoreCase(getResources().getString(
				R.string.most_recent))) {
			Collections.sort(electricMobilesItems, mostRecentComparator);
			setDataIntoListView(electricMobilesItems);
		} else if (sortType.equalsIgnoreCase(getResources().getString(
				R.string.low_price))) {
			Collections.sort(electricMobilesItems, lowPriceComparator);
			setDataIntoListView(electricMobilesItems);
		} else if (sortType.equalsIgnoreCase(getResources().getString(
				R.string.high_price))) {
			Collections.sort(electricMobilesItems, highPriceComparator);
			setDataIntoListView(electricMobilesItems);
		} else if (sortType.equalsIgnoreCase(getResources().getString(
				R.string.with_picture))) {
			setDataWithPictures();
		}

	}

	/**
	 * Method perform action click on set with pictures........
	 */
	private void setDataWithPictures() {
		ArrayList<AdItem> adsWithPictures = new ArrayList<AdItem>();
		for (int i = 0; i < electricMobilesItems.size(); i++) {
			AdItem item = electricMobilesItems.get(i);
			if (item.images.size() > 0) {
				adsWithPictures.add(item);
			}
		}
		if (adsWithPictures.size() > 0)
			setDataIntoListView(adsWithPictures);
		else {
			setDataIntoListView(adsWithPictures);
			Util.showToastMessage(getApplicationContext(), getResources()
					.getString(R.string.error_with_picture));
		}
	}

	static Comparator<AdItem> mostRecentComparator = new Comparator<AdItem>() {
		@Override
		public int compare(AdItem lhs, AdItem rhs) {
			if (lhs.dateAndTime == null || rhs.dateAndTime == null)
				return 0;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddhh:mm:ss");
			Date d1 = null;
			Date d2 = null;
			try {
				d1 = sdf.parse(lhs.dateAndTime);
				d2 = sdf.parse(rhs.dateAndTime);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return (d1.getTime() > d2.getTime() ? -1 : 1);
		}
	};

	static Comparator<AdItem> lowPriceComparator = new Comparator<AdItem>() {
		@Override
		public int compare(AdItem lhs, AdItem rhs) {
			long leftLowPrice, rightLowPrice;
			if (lhs.lowPrice.equalsIgnoreCase("")) {
				leftLowPrice = 0;
			} else {
				leftLowPrice = Long.parseLong(lhs.lowPrice);
			}
			if (rhs.lowPrice.equalsIgnoreCase("")) {
				rightLowPrice = 0;
			} else {
				rightLowPrice = Long.parseLong(rhs.lowPrice);
			}
			return (leftLowPrice > rightLowPrice ? 1 : -1);
		}
	};

	static Comparator<AdItem> highPriceComparator = new Comparator<AdItem>() {
		@Override
		public int compare(AdItem lhs, AdItem rhs) {
			long leftHighPrice;
			long rightHighPrice;
			if (lhs.lowPrice.equalsIgnoreCase("")) {
				leftHighPrice = 0;
			} else {
				leftHighPrice = Long.parseLong(lhs.lowPrice);
			}

			if (rhs.lowPrice.equalsIgnoreCase("")) {
				rightHighPrice = 0;
			} else {
				rightHighPrice = Long.parseLong(rhs.lowPrice);
			}
			return (rightHighPrice > leftHighPrice ? 1 : -1);
		}
	};

	/**
	 * Method perform action after reset filter dialog.....
	 * @param location
	 * @param range
	 * @param adType
	 * @param title
	 */
	public void resetFilterLayout(String location, String range, String adType,
			String title) {
		locationFilter = location;
		// rangeFilter = range.trim();
		rangeFilter = range;
		this.title = title;
		adTypeFilter = adType;
		sortLinearLayout.setVisibility(View.GONE);
		for (int i = 0; i < headerLinearLayout.getChildCount(); i++) {
			headerLinearLayout.getChildAt(i).setEnabled(true);
		}
		electricMobileList.setEnabled(true);
		filterAdsList();

	}

	/**
	 * Method to set filter ads lists.........
	 */
	private void filterAdsList() {

		String[] locations = null;
	    //String[] titles = null;
		if (!locationFilter.equals(""))
			locations = locationFilter.split(",");

		//if (!title.equals(""))
		//	titles = title.split(",");

		String[] amountRange = null;
		if (!rangeFilter.equals(""))
			amountRange = rangeFilter.split("to");
		ArrayList<AdItem> filteredAdsItems = new ArrayList<AdItem>();
		ArrayList<AdItem> allFilteredAdsItems = new ArrayList<AdItem>();
		for (int i = 0; i < electricMobilesItems.size(); i++) {
			AdItem item = electricMobilesItems.get(i);
			if (!title.equalsIgnoreCase("") && locations != null && amountRange != null) {

				for (int j = 0; j < locations.length; j++) {
					if (item.title.equalsIgnoreCase(title.trim())
							&& item.city.equalsIgnoreCase(locations[j].trim())
							&& item.status.equalsIgnoreCase(adTypeFilter)
							&& Integer.parseInt(item.lowPrice) >= Integer
									.parseInt(amountRange[0].trim())
							&& Integer.parseInt(item.lowPrice) <= Integer
									.parseInt(amountRange[1].trim())) {
						allFilteredAdsItems.add(item);
					}
				}
			

			} else if (locations != null && amountRange == null
					&& !title.equalsIgnoreCase("")) {

				for (int j = 0; j < locations.length; j++) {
					if (item.title.equalsIgnoreCase(title.trim())
							&& item.city.equalsIgnoreCase(locations[j].trim())
							&& item.status.equalsIgnoreCase(adTypeFilter)) {
						allFilteredAdsItems.add(item);
					}
				}
			
			} else if (locations == null && amountRange != null
					&& !title.equalsIgnoreCase("")) {

					if (item.status.equalsIgnoreCase(adTypeFilter)
							&& item.title.equalsIgnoreCase(title.trim())
							&& Integer.parseInt(item.lowPrice) >= Integer
									.parseInt(amountRange[0].trim())
							&& Integer.parseInt(item.lowPrice) <= Integer
									.parseInt(amountRange[1].trim())) {
						allFilteredAdsItems.add(item);
					}
	
			} else if (locations != null && amountRange != null
					&& title.equalsIgnoreCase("")) {

				for (int j = 0; j < locations.length; j++) {
					if (item.status.equalsIgnoreCase(adTypeFilter)
							&& item.city.equalsIgnoreCase(locations[j].trim())
							&& Integer.parseInt(item.lowPrice) >= Integer
									.parseInt(amountRange[0].trim())
							&& Integer.parseInt(item.lowPrice) <= Integer
									.parseInt(amountRange[1].trim())) {
						allFilteredAdsItems.add(item);
					}

				}
			} else if (locations == null && amountRange == null
					&& title.equalsIgnoreCase("")) {
				if (item.status.equalsIgnoreCase(adTypeFilter)) {
					allFilteredAdsItems.add(item);

				}
			} else if (locations == null && amountRange != null
					&& title.equalsIgnoreCase("")) {
				if (item.status.equalsIgnoreCase(adTypeFilter)
						&& Integer.parseInt(item.lowPrice) >= Integer
								.parseInt(amountRange[0].trim())
						&& Integer.parseInt(item.lowPrice) <= Integer
								.parseInt(amountRange[1].trim())) {
					allFilteredAdsItems.add(item);
				}
			}
		}

		setDataIntoListView(allFilteredAdsItems);

	}

	
	
	public void likeUnlikeActionSubCategories(AdItem item1, int position){
		
		if(sortLinearLayout.getVisibility() == View.GONE) {
		
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(getApplicationContext())) {
				checkAdItem = item1;
				checkPosition = position;
				if(mGoogleApiClient!=null)
				{
					showDialogForSignInGoogle(new DialogButtonListener() {
						
						@Override
						public void onButtonClicked(String text) {
							if(text.equals("No")){
								//finish();
							}else{
								if (mGoogleApiClient != null){
									startBusy();
									mGoogleApiClient.connect();
									
								}
							}
							
						}
					});
				}
				else
				{
					setGoogleObject();
					mGoogleApiClient.connect();
					
				}
				
				
				/*setGoogleObject();
				if (mGoogleApiClient != null
						&& !mGoogleApiClient.isConnecting()) {
					startBusy();
					mSignInClicked = true;
					resolveSignInError();
				}*/
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			
			NKApplication.getInstance().selectedItem = listAds
					.get(position);
			AdItem item = NKApplication.getInstance().selectedItem;
			
			if(item.email.equals(MyPrefs.getInstance().email)){
				String path = db.getPathByAdId(item.adItemId);
				Logger.logFile = new File(path);
				if (item.isfavourite) {
					item.isfavourite = false;
				} else {
					item.isfavourite = true;
				}
				item.update(SubCategories.this, SubCategories.this);
			} else {
				Util.showToastMessage(SubCategories.this, getResources().getString(R.string.non_user_like_unlike_text));
			}
			
		}
		
		}
		
		
	}
	
	public void checkLikeAfterGoogleLogin(){
	
		likeUnlikeActionSubCategories(checkAdItem,checkPosition);
	}
	
	public void setSubCatAdGoogleData() {

		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {

			if (Util.isNetworkAvailable(getApplicationContext())) {
				startBusy();
				
					if (mGoogleApiClient != null
							&& !mGoogleApiClient.isConnecting()) {
						mSignInClicked = true;
						resolveSignInError();
					}
					else {
						Util.showToastMessage(getApplicationContext(), getResources()
								.getString(R.string.internet_error));
					}
				
			
				
			} 
		} else {
			//showDialogForSucessFulSignInGoogle();
			checkLikeAfterGoogleLogin();
		}

	}
	
}
