package com.nkaplication;

import java.io.File;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.nkaplication.adp.LocalitiesAdapter;
import com.nkaplication.adp.SelectPlaceNameAdapter;
import com.nkaplication.db.NKDB;
import com.nkaplication.model.AdItem;
import com.nkaplication.model.BaseModel;
import com.nkaplication.model.EditAdsMyRowModel;
import com.nkaplication.model.SelectPlaceNameModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.ConfigUtil;
import com.nkaplication.util.Constants;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Logger;
import com.nkaplication.util.NKLocationListener;
import com.nkaplication.util.Util;

/**
 * @author brijesh dutt
 * 
 *         this Activity will be open from Home Activity , It is for post ads
 * 
 */

public class PostAd extends BaseActivity implements DataListener, Constants,
		OnClickListener {

	private LinearLayout mobilesImagesLayout;
	private DrawerLayout drawer_layout;
	private ListView postAdsList;
	private EditText postAdsEmail, postAdsTitle, postAdsDescription,
			postAdsMobile, postAdsLowPrice, postAdsUserName, postAdsHighPrice;
	private LinearLayout postAdsSuccessfullId, postAdsSellBuyLayout;
	private ScrollView postAdsCompleteLayoutId;
	private String email, cate, subcate, title;
	private RadioGroup postAdsBuySellRadioGroup, postAdsConditionRadioGroup;
	private RadioButton adsTypeRadioButton, conditionRadioButton,
			newRadioButton, usedRadioButton;
	private Spinner categorySpinner, subCategorySpinner;
	private ImageView emailIsCheckedId, mobileIsCheckedId, negotiableImageId;
	private ArrayList<EditAdsMyRowModel> imagesCaptured = new ArrayList<EditAdsMyRowModel>();
	private TextView postAdsSellBuyId, mobileIsVisibleTextId,
			emailIsVisibleTextId, postAdsSuccessfullTextView, localityTextView,
			cityTextView;
	private Bitmap thumbnail;
	private String[] categoryArray;
	private boolean isMobileChecked = false, isEmailChecked = false,
			isNegotiableChecked = false;
	private ArrayList<String> imageUrls = new ArrayList<String>();
	private ImageButton postAddPhotoImageCapture;
	private int imageCount;
	private LinearLayout emailCheckedLinearLayout, mobileCheckedLinearLayout,
			negotiableLinearLayoutId, postAdsHighPriceLinearLayoutId;
	private LinearLayout locality, city;
	private ArrayList<SelectPlaceNameModel> selectPlaceNameList = new ArrayList<SelectPlaceNameModel>();
	private ArrayList<SelectPlaceNameModel> newArrayList = new ArrayList<SelectPlaceNameModel>();
	private ArrayList<SelectPlaceNameModel> selectCityNameList = new ArrayList<SelectPlaceNameModel>();
	private ArrayList<SelectPlaceNameModel> cityList = new ArrayList<SelectPlaceNameModel>();

	private LocalitiesAdapter selectPlaceAdapter;
	private Dialog d;
	private String isCityOrLocality;
	private String cityspi;
	private String locali;
	private AdItem entereditem;
	private TextView postAdsStatusTypeText;
	private SelectPlaceNameAdapter selectLocalLityAdapter;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.post_ad_main);

		NKApplication.getInstance().requestLocation(new NKLocationListener() {
			
			@Override
			public void onLocation(Location c) {
				
				if(c != null){
				boolean isExist = setCityListData(Util.getCity(c.getLatitude(), c.getLongitude()));
			//	setLocalityListData();
				if (isExist)
					cityTextView.setText(Util.getCity(c.getLatitude(), c.getLongitude()));
				else
					cityTextView
							.setText(getResources().getString(R.string.select_city));
				} else {
					Util.showToastMessage(PostAd.this, getResources().getString(R.string.location_not_found_text));
					setCityListData(Util.getCity(25.33, 77.366));
					cityTextView.setText(getResources().getString(R.string.select_city));
				}
				 
				
			}
		});
	
		ctx = this;
		postAdsList = (ListView) findViewById(R.id.postAdsList);
		drawer_layout = (DrawerLayout) findViewById(R.id.post_ads_drawer_layout);
		mobilesImagesLayout = (LinearLayout) findViewById(R.id.postAdMobilesImagesLayout);
		menuListButton = (ImageView) findViewById(R.id.postAdsMenuIcon);
		postAdsEmail = (EditText) findViewById(R.id.postAdsEmail);
		postAdsTitle = (EditText) findViewById(R.id.postAdsTitle);
		postAdsDescription = (EditText) findViewById(R.id.postAdsDescription);
		postAdsMobile = (EditText) findViewById(R.id.postAdsMobile);
		postAdsLowPrice = (EditText) findViewById(R.id.postAdsLowPrice);
		postAdsHighPrice = (EditText) findViewById(R.id.postAdsHighPrice);
		postAdsUserName = (EditText) findViewById(R.id.postAdsUserName);
		postAdsSuccessfullId = (LinearLayout) findViewById(R.id.postAdsSuccessfullId);
		postAdsCompleteLayoutId = (ScrollView) findViewById(R.id.postAdsCompleteLayoutId);
		postAdsBuySellRadioGroup = (RadioGroup) findViewById(R.id.postAdsBuySellRadioGroup);
		postAdsConditionRadioGroup = (RadioGroup) findViewById(R.id.postAdsConditionRadioGroup);
		postAdsStatusTypeText = (TextView) findViewById(R.id.postAdsStatusTypeText);
		newRadioButton = (RadioButton) findViewById(R.id.postAdNewRadioButtonId);
		usedRadioButton = (RadioButton) findViewById(R.id.postAdUsedRadioButtonId);
		negotiableLinearLayoutId = (LinearLayout) findViewById(R.id.negotiableLinearLayoutId);
		negotiableImageId = (ImageView) findViewById(R.id.negotiableImageId);
		postAdsHighPriceLinearLayoutId = (LinearLayout) findViewById(R.id.postAdsHighPriceLinearLayoutId);
		postAdsSellBuyLayout = (LinearLayout) findViewById(R.id.postAdsSellBuyLayout);
		postAdsSellBuyId = (TextView) findViewById(R.id.postAdsSellBuyId);
		city = (LinearLayout) findViewById(R.id.postAdCityId);
		locality = (LinearLayout) findViewById(R.id.postAdLocalityId);
		categorySpinner = (Spinner) findViewById(R.id.postAdCategoryId);
		subCategorySpinner = (Spinner) findViewById(R.id.postAdSubcategoryId);
		emailIsCheckedId = (ImageView) findViewById(R.id.emailIsCheckedId);
		emailCheckedLinearLayout = (LinearLayout) findViewById(R.id.emailCheckedLinearLayout);
		mobileIsCheckedId = (ImageView) findViewById(R.id.mobileIsCheckedId);
		mobileIsVisibleTextId = (TextView) findViewById(R.id.mobileIsVisibleTextId);
		emailIsVisibleTextId = (TextView) findViewById(R.id.emailIsVisibleTextId);
		postAddPhotoImageCapture = (ImageButton) findViewById(R.id.postAddPhotoImageCapture);
		postAdsSuccessfullTextView = (TextView) findViewById(R.id.postAdsSuccessfullTextView);
		mobileCheckedLinearLayout = (LinearLayout) findViewById(R.id.mobileCheckedLinearLayout);
		localityTextView = (TextView) findViewById(R.id.localityTextView);
		cityTextView = (TextView) findViewById(R.id.cityTextView);
		
		postAdsLowPrice.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {
					long price = Long.parseLong(s.toString());
					if (s.length() == 5
							&& Integer.parseInt("" + s.charAt(0)) > 1) {
						postAdsLowPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										5) });
						Util.showToastMessage(PostAd.this, getResources()
								.getString(R.string.max_price_error));
					} else if (price < ConfigUtil.MAX_PRICE_RANGE) {
						postAdsLowPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										6) });
					} else if (price == ConfigUtil.MAX_PRICE_RANGE) {
						postAdsLowPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										6) });
						Util.showToastMessage(PostAd.this, getResources()
								.getString(R.string.max_price_error));
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		postAdsHighPrice.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {
					long price = Long.parseLong(s.toString());
					if (s.length() == 5
							&& Integer.parseInt("" + s.charAt(0)) > 1) {
						postAdsHighPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										5) });
						Util.showToastMessage(PostAd.this, getResources()
								.getString(R.string.max_price_error));
					} else if (price < ConfigUtil.MAX_PRICE_RANGE) {
						postAdsHighPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										6) });
					} else if (price == ConfigUtil.MAX_PRICE_RANGE) {
						postAdsHighPrice
								.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
										6) });
						Util.showToastMessage(PostAd.this, getResources()
								.getString(R.string.max_price_error));
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		// /////////////////////////////////////////////////

		postAdsTitle.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {

					postAdsTitle.setHintTextColor(getResources().getColor(
							R.color.black));

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				postAdsTitle.setHintTextColor(getResources().getColor(
						R.color.black));

			}
		});

		postAdsDescription.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {

					postAdsDescription.setHintTextColor(getResources()
							.getColor(R.color.black));

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				postAdsDescription.setHintTextColor(getResources().getColor(
						R.color.black));

			}
		});

		localityTextView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {

					localityTextView.setTextColor(getResources().getColor(
							R.color.black));

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				localityTextView.setTextColor(getResources().getColor(
						R.color.black));

			}
		});

		cityTextView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() >= 1) {

					cityTextView.setTextColor(getResources().getColor(
							R.color.black));

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cityTextView.setTextColor(getResources()
						.getColor(R.color.black));

			}
		});

		emailCheckedLinearLayout.setOnClickListener(this);
		mobileCheckedLinearLayout.setOnClickListener(this);

		negotiableLinearLayoutId.setOnClickListener(this);
		getStringArrays();
		setArraysOnAllSpinners();

		locality.setOnClickListener(this);
		city.setOnClickListener(this);
		postAdsBuySellRadioGroup
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						postAdsStatusTypeText.setTextColor(getResources().getColor(R.color.black));
						postAdsLowPrice.setHintTextColor(getResources().getColor(R.color.black));
						postAdsHighPrice.setHintTextColor(getResources().getColor(R.color.black));
			            for (int i = 0; i < group.getChildCount(); i++) {
			            	View o = group.getChildAt(i);
							((RadioButton)o).setTextColor(getResources().getColor(R.color.black));
						}
						RadioButton TheTextIsHere = (RadioButton) findViewById(group
								.getCheckedRadioButtonId());
						String str = TheTextIsHere.getText().toString();
						if (str.equalsIgnoreCase(KEY_SELL)) {
							postAdsHighPriceLinearLayoutId
									.setVisibility(View.INVISIBLE);
							negotiableLinearLayoutId
									.setVisibility(View.VISIBLE);
							postAdsSellBuyLayout.setVisibility(View.VISIBLE);
							postAdsSellBuyId.setText(getResources().getString(
									R.string.sell_colon));
							postAdsLowPrice.setText("");
							postAdsLowPrice.setHint(getResources().getString(
									R.string.price_noColon));

							return;
						}
						if (str.equalsIgnoreCase(KEY_BUY)) {
							postAdsSellBuyLayout.setVisibility(View.VISIBLE);
							negotiableLinearLayoutId
									.setVisibility(View.INVISIBLE);
							postAdsSellBuyId.setText(getResources().getString(
									R.string.buy_colon));
							postAdsLowPrice.setText("");
							postAdsLowPrice.setHint(getResources().getString(
									R.string.low_price));
							postAdsHighPriceLinearLayoutId
									.setVisibility(View.VISIBLE);

							postAdsHighPrice.setText("");
							postAdsHighPrice.setHint(getResources().getString(
									R.string.high_price));
							return;
						}

					}
				});

		
		drawer_layout.setDrawerListener(this);
		
	}

	/**
	 * method of menu slider
	 */

	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setHeaderWithSlider();
	}
	
	/**
	 * Method for showing menu slider ......
	 */
	private void setHeaderWithSlider() {
		initActionBar(drawer_layout, postAdsList);
		drawer_layout.setDrawerListener(this);
	}

	/**
	 * Perform action click on any item of MenuList slider
	 */

	public void performAdsPost(View v) {
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(PostAd.this)) {
				
				
				if(mGoogleApiClient == null)
				{
					setGoogleObject();
					
				}
				
				mGoogleApiClient.connect();
				checkLoggedIn = false;
				//setGoogleObject();
				if (mGoogleApiClient != null
						&& !mGoogleApiClient.isConnected()) {
				//	startBusy();
				//	mSignInClicked = true;
				//	resolveSignInError();
				}
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			postDatalist();
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(PostAd.this)) {
				
				if(mGoogleApiClient != null){
					showDialogForSignInGoogle(new DialogButtonListener() {
						
						@Override
						public void onButtonClicked(String text) {
							if(text.equals("No")){
								finish();
							}else{
								
								if (mGoogleApiClient != null){
									startBusy();
									mGoogleApiClient.connect();
								}
							}
							
						}
					});
				} else {
					//setGoogleObject();
					//mGoogleApiClient.connect();
				}
				
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else
			setEmailOnId();
	}

	/**
	 * Method for set email and username..........
	 */
	public void setEmailOnId() {
		postAdsEmail.setText(MyPrefs.getInstance().email.trim());
		postAdsUserName.setText(MyPrefs.getInstance().userFirstName + " "
				+ MyPrefs.getInstance().userLastName);
		setHeaderWithSlider();
	}

	/**
	 * Method for set post ad data..........
	 */
	public void postDatalist() {
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(PostAd.this)) {
				showDialogForSignInGoogle(new DialogButtonListener() {
					
					@Override
					public void onButtonClicked(String text) {
						if(text.equals("No")){
							finish();
						}else{
							if (mGoogleApiClient != null){
								startBusy();
								mGoogleApiClient.connect();
							}
						}
						
					}
				});
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
			
			return ;
		}
		AdItem item = new AdItem();
		int selectedAdsTypeId = postAdsBuySellRadioGroup
				.getCheckedRadioButtonId();
		adsTypeRadioButton = (RadioButton) findViewById(selectedAdsTypeId);
		int selectedConditionId = postAdsConditionRadioGroup
				.getCheckedRadioButtonId();
		conditionRadioButton = (RadioButton) findViewById(selectedConditionId);
		title = postAdsTitle.getEditableText().toString();
		item.title = title;
		item.lowPrice = postAdsLowPrice.getEditableText().toString();
		item.highPrice = postAdsHighPrice.getEditableText().toString();
		item.description = postAdsDescription.getEditableText().toString();
		cityspi = cityTextView.getText().toString();
		if (!cityspi.equalsIgnoreCase(getResources().getString(
				R.string.select_city)))
			item.city = cityspi;

		locali = localityTextView.getText().toString();
		if (!locali.equalsIgnoreCase(getResources().getString(
				R.string.select_locality)))
			item.district = locali;
		if (conditionRadioButton != null
				&& conditionRadioButton.getText() != null)
			item.condition = conditionRadioButton.getText().toString();
		cate = categorySpinner.getSelectedItem().toString().toLowerCase();
		subcate = subCategorySpinner.getSelectedItem().toString().toLowerCase();
		if (!cate.equalsIgnoreCase("select category"))
			item.category = cate;
		if (!subcate.equalsIgnoreCase("First Select Category"))
			item.subcategory = subcate;
		if (adsTypeRadioButton != null && adsTypeRadioButton.getText() != null)
			item.status = adsTypeRadioButton.getText().toString();
		email = postAdsEmail.getEditableText().toString().trim();
		item.email = email;
		item.mobile = postAdsMobile.getEditableText().toString();
		item.nameUser = postAdsUserName.getEditableText().toString();
		item.dateAndTime = Util.getCurrentDateTime();
		item.isfavourite = false;
		// item.images.addAll(imagesCaptured);
		item.isMobileVisible = isMobileChecked;
		item.isEmailVisible = isEmailChecked;
		item.isNegotiable = isNegotiableChecked;
		for (int i = 0; i < imageUrls.size(); i++) {
			item.images.add(imageUrls.get(i));
		}

		checkValidation(item);

	}

	/**
	 * Method to check validation field before create ad....
	 * @param item
	 */
	private void checkValidation(AdItem item) {

		boolean isAllFilled = false;
		if (!Util.isNull(item.title)) {
			/*
			 * Util.showToastMessage(PostAd.this,
			 * getResources().getString(R.string.enter_title));
			 */

			postAdsTitle.setHintTextColor(getResources().getColor(R.color.red));
			isAllFilled = true;
		}
		if (!Util.isNull(item.description)) {

			postAdsDescription.setHintTextColor(getResources().getColor(
					R.color.red));
			isAllFilled = true;
		}

		if (cityspi.equalsIgnoreCase(getResources().getString(
				R.string.select_city))) {
			cityTextView.setTextColor(getResources().getColor(R.color.red));
			isAllFilled = true;
		}
		if (!Util.isNull(item.district)) {

			localityTextView.setTextColor(getResources().getColor(R.color.red));
			isAllFilled = true;
		}
		if (cate.equalsIgnoreCase(getResources().getString(
				R.string.select_category))) {

			TextView tv = (TextView) findViewById(R.id.spinnerTextId);
			tv.setTextColor(getResources().getColor(R.color.red));

			isAllFilled = true;
		}
		if (subcate.equalsIgnoreCase(getResources().getString(
				R.string.select_sub_category))
				|| subcate.equalsIgnoreCase(getResources().getString(
						R.string.select))) {
			TextView tv = (TextView) findViewById(R.id.subCatSpinnerTextId);
			tv.setTextColor(getResources().getColor(R.color.red));
			isAllFilled = true;
		}
		if (item.status == null) {
			postAdsStatusTypeText.setTextColor(getResources().getColor(R.color.red));
			for (int i = 0; i < postAdsBuySellRadioGroup.getChildCount(); i++) {
				View o = postAdsBuySellRadioGroup.getChildAt(i);
				((RadioButton)o).setTextColor(getResources().getColor(R.color.red));
			}
			isAllFilled = true;
		}
		if(item.lowPrice.equals(""))
		{
			postAdsLowPrice.setHintTextColor(getResources().getColor(R.color.red));
			isAllFilled = true;
		}
		if(item.status == null){
			isAllFilled = true;
		} else {
			if(item.highPrice.equals("") && item.status.equalsIgnoreCase(KEY_BUY))
			{
				postAdsHighPrice.setHintTextColor(getResources().getColor(R.color.red));
				isAllFilled = true;
			}
		}
		if (!isAllFilled) {
			Logger.logFile = null;
			entereditem = item;
			item.storeData(PostAd.this, PostAd.this);
		} else {
			Util.showToastMessage(PostAd.this,
					getResources().getString(R.string.mandatory_fields));
		}
	}

	@Override
	public void onDataResponse(boolean isError, Response r, final int rid,
			Object data) {
		h.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (rid == BaseModel.REQUEST_GET_ALL_ADS_LIST) {

						NKDB db = new NKDB(PostAd.this);
						db.insertProfilePath(MyPrefs.getInstance().email,
								(Logger.logFile).toString(), cate, subcate);
						int id = db.getIdByFilePath((Logger.logFile).toString());
						
						entereditem.adItemId = id;
						Logger.logFile = new File((Logger.logFile).toString());
						
						updateFileWithAdId(entereditem);
						
						
					}
					
					if(rid == BaseModel.REQUEST_UPDATE__AD){
						stopBusy();
						postAdsCompleteLayoutId.setVisibility(View.GONE);
						postAdsSuccessfullId.setVisibility(View.VISIBLE);

						if (entereditem != null)
							NKApplication.getInstance().selectedItem = entereditem;
						// setTextViewHTML(postAdsSuccessfullTextView,
						// "Your Ad <b><u><a href='ItemDetail'>"
						// + title
						// +
						// "</a></u></b> has been posted successfully, It will go live shortly.");

						SpannableString ss = new SpannableString(
								Html.fromHtml("Your Ad "
										+ title
										+ " has been posted successfully, It will go live shortly."));
						ClickableSpan clickableSpan = new ClickableSpan() {
							@Override
							public void onClick(View textView) {
								finish();
								startActivity(new Intent(PostAd.this,
										ItemDetail.class));
							}

							@Override
							public void updateDrawState(TextPaint ds) {
								super.updateDrawState(ds);
								ds.setUnderlineText(true);
							}
						};
						ss.setSpan(clickableSpan, 8, title.length() + 8,
								Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

						postAdsSuccessfullTextView.setText(ss);
						postAdsSuccessfullTextView
								.setMovementMethod(LinkMovementMethod
										.getInstance());
						Util.hideSoftKeyboard(PostAd.this);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			
		});
	}

	
	/**
	 * Method for update file of ad by id..............
	 * @param entereditem
	 */
	private void updateFileWithAdId(AdItem entereditem) {
		entereditem.update(PostAd.this, PostAd.this);
	}
	/**
	 * Method for set spinner data.......
	 * @param array
	 */
	public void setSpinnerOnAdp(String[] array) {
		

		// String array[] = { "one", "two", "three" };
		ArrayAdapter<String> sp_adapter = new ArrayAdapter<String>(this,
				R.layout.subcat_spinner_text, array);
		sp_adapter.setDropDownViewResource(R.layout.subcat_spinner_selector);
		// Spinner sp = (Spinner) findViewById(R.id.spinner1);
		subCategorySpinner.setAdapter(sp_adapter);
		subCategorySpinner.setEnabled(true);
		subCategorySpinner.setClickable(true);
	}

	/**
	 * Method for add photos camera action..............
	 * @param v
	 */
	public void performAddPhotosCamera(View v) {
		if (imagesCaptured.size() < 4)
			Util.showMediaDialogAddPace(this, this);
		else {
			// addPhotoImageCapture.setVisibility(View.GONE);
			Util.showToastMessage(this,
					getResources().getString(R.string.capture_four_image));
		}
	}

	/**
	 * Method for pick up image from gallery.........
	 */
	public void openGallery() {

		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		startActivityForResult(i, 2);

	}

	/**
	 * Method for capture image from camera.......
	 */
	public void openCamera() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(cameraIntent, 1);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == 1) {
				// imageStoreInSdCard();

				thumbnail = (Bitmap) data.getExtras().get("data");
				File file = Util.getFileStoredCurrentBitmap(thumbnail);
				if(imageUrls.size() < 4)
				imageUrls.add(file.getAbsolutePath());
				if (thumbnail != null) {
					EditAdsMyRowModel bitmapCaption = new EditAdsMyRowModel();
					bitmapCaption.caption = thumbnail;
					imagesCaptured.add(bitmapCaption);
					inflateImagesOnLayout();
					if (imagesCaptured.size() < 4)
						postAddPhotoImageCapture.setVisibility(View.VISIBLE);
					else
						postAddPhotoImageCapture.setVisibility(View.GONE);
				}

			}
			if (requestCode == 2) {

				thumbnail = Util.getBitmapOfGellaryImage(PostAd.this, data);
				File file = Util.getFileStoredCurrentBitmap(thumbnail);
				if(imageUrls.size() < 4)
				imageUrls.add(file.getAbsolutePath());
				if (thumbnail != null) {
					EditAdsMyRowModel bitmapCaption = new EditAdsMyRowModel();
					bitmapCaption.caption = thumbnail;
					imagesCaptured.add(bitmapCaption);
					inflateImagesOnLayout();
					if (imagesCaptured.size() < 4)
						postAddPhotoImageCapture.setVisibility(View.VISIBLE);
					else
						postAddPhotoImageCapture.setVisibility(View.GONE);
				}

			}

		}
	}

	/**
	 * Method for get categories array.....
	 */
	private void getStringArrays() {
		
		categoryArray = getResources().getStringArray(R.array.category);
	}

	/**
	 * Method for set array for all spinners..........
	 */
	private void setArraysOnAllSpinners() {
		
		// ///////////////////////////////
		ArrayAdapter<String> sp_adapter = new ArrayAdapter<String>(this,
				R.layout.spinnertext, categoryArray);
		sp_adapter.setDropDownViewResource(R.layout.spinner_selector);
		// Spinner sp = (Spinner) findViewById(R.id.spinner1);
		categorySpinner.setAdapter(sp_adapter);
		categorySpinner.setEnabled(true);
		categorySpinner.setClickable(true);

		clickOnCategorySpinnerItem();
	}

	/**
	 * Method for perform action click on category spinner........
	 */
	private void clickOnCategorySpinnerItem() {
		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View view,
					int arg2, long arg3) {

				String selectedCategoryItem = categorySpinner.getSelectedItem()
						.toString();

				if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_ELECTRONICS)) {
					setSpinnerOnAdp(getResources().getStringArray(
							R.array.electronics_item));

				} else if (selectedCategoryItem.toLowerCase().equalsIgnoreCase(
						KEY_AUTO_MOBILES)) {
					setSpinnerOnAdp(getResources().getStringArray(
							R.array.autoMobilesItem));

				} else if (selectedCategoryItem
						.equalsIgnoreCase(KEY_REAL_ESTATE)) {
					setSpinnerOnAdp(getResources().getStringArray(
							R.array.realEstate));

				} else if (selectedCategoryItem
						.equalsIgnoreCase(KEY_HOME_AND_FURNITURE)) {

					setSpinnerOnAdp(getResources().getStringArray(
							R.array.homeAndFurniture));
				} else {
					setSpinnerOnAdp(new String[] { "Select SubCategory" });
					subCategorySpinner.setEnabled(false);
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * Method for inflate images after select from gallery or camera.......
	 */
	protected void inflateImagesOnLayout() {

		mobilesImagesLayout.removeAllViews();
		LayoutInflater inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (imageCount = 0; imageCount < imagesCaptured.size(); imageCount++) {
			// EditAdsMyRowModel imageAds = adsItems.get(i);
			final View v = inflate.inflate(R.layout.edit_my_ads_row, null);
			ImageView myeditAdsRowImageIcon = (ImageView) v
					.findViewById(R.id.myeditAdsRowImageIcon);
			ImageView cancelImage = (ImageView) v
					.findViewById(R.id.cancelImage);
			cancelImage.setTag("" + imageCount);
			Bitmap image;
			try {
				// AssetManager mngr = context.getAssets();
				// InputStream ims = mngr.open(item.imageIcon);
				// InputStream ims = this.getAssets().open(imageAds.imageIcon);
				// image = BitmapFactory.decodeStream(ims);
				myeditAdsRowImageIcon.setImageBitmap(imagesCaptured
						.get(imageCount).caption);
				// System.out.println(image);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			cancelImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					String tag = view.getTag().toString();
					imagesCaptured.remove(Integer.parseInt(tag));
					if (imagesCaptured.size() < 4)
						postAddPhotoImageCapture.setVisibility(View.VISIBLE);
					else
						postAddPhotoImageCapture.setVisibility(View.GONE);
					inflateImagesOnLayout();
				}
			});

			mobilesImagesLayout.addView(v);
		}
	}

	/* 
	 * Method for performing click action on different views........
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (v == emailCheckedLinearLayout) {
			if (isEmailChecked) {
				isEmailChecked = false;
				emailIsCheckedId.setImageResource(R.drawable.uncheck);
				
			} else {
				isEmailChecked = true;
				emailIsCheckedId.setImageResource(R.drawable.check);
				
			}
		} else if (v == mobileCheckedLinearLayout) {
			if (isMobileChecked) {
				isMobileChecked = false;
				mobileIsCheckedId.setImageResource(R.drawable.uncheck);
				
			} else {
				isMobileChecked = true;
				mobileIsCheckedId.setImageResource(R.drawable.check);
				
			}
		} else if (v == locality) {
			isCityOrLocality = getResources().getString(R.string.locality);
			
			String location = localityTextView.getText().toString();
			showLocationAndTitleDialog(location,true);
		} else if (v == city) {
			isCityOrLocality = getResources().getString(R.string.city);
			showLocationDialog(cityList, selectCityNameList);
		} else if (v == negotiableLinearLayoutId) {
			if (isNegotiableChecked) {
				isNegotiableChecked = false;
				negotiableImageId.setImageResource(R.drawable.uncheck);

			} else {
				isNegotiableChecked = true;
				negotiableImageId.setImageResource(R.drawable.check);
			}
		}
	}
	
	
	

	/**
	 * Method to open dialog for location selection................
	 * @param list
	 * @param selectedList
	 */
	public void showLocationDialog(final ArrayList<SelectPlaceNameModel> list,
			final ArrayList<SelectPlaceNameModel> selectedList) {
		d = new Dialog(PostAd.this, R.style.FullHeightDialog);
		d.setContentView(R.layout.select_place_name);
		d.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		d.getWindow().setGravity(Gravity.CENTER);
		// adding text dynamically
		EditText placeNameEditText = (EditText) d
				.findViewById(R.id.placeNameEditText);
		ListView placeNameListView = (ListView) d
				.findViewById(R.id.placeNameListView);
		placeNameListView.setDivider(getResources().getDrawable(
				R.drawable.devider));
		Button applyLocationBtn = (Button) d
				.findViewById(R.id.applyLocationBtn);
		applyLocationBtn.setVisibility(View.GONE);
		// placeNameListView.setItemsCanFocus(false);
		placeNameListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		placeNameListView.setTextFilterEnabled(true);
		// setLocationAdapter(selectPlaceNameList, placeNameListView);
		selectPlaceAdapter = new LocalitiesAdapter(this, list);
		placeNameListView.setAdapter(selectPlaceAdapter);

		placeNameEditText.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				list.clear();
				for (int i = 0; i < selectedList.size(); i++) {
					if ((selectedList.get(i).placeName.toString().toLowerCase())
							.indexOf(s.toString().toLowerCase()) > -1) {
						list.add(selectedList.get(i));
					}
				}
				selectPlaceAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		d.show();

	}

	/**
	 * Method for set cities list data by city...........
 	 * @param city
	 * @return
	 */
	private boolean setCityListData(String city) {

		boolean isExist = false;
		String citiesFile = Logger.readAssetsCitiesFile(ctx, "cities.txt");
		String[] cities = citiesFile.split(",");

		for (int i = 0; i < cities.length; i++) {
			if (city.equalsIgnoreCase(cities[i])) {
				isExist = true;
			} else {
				SelectPlaceNameModel sp = new SelectPlaceNameModel();
				sp.placeName = cities[i];
				selectCityNameList.add(sp);
				cityList.add(sp);
			}
		}
		return isExist;

	}
///////////////////////////////////////////////////////////
	/*private void setLocalityListData() {
		String[] localities = getResources().getStringArray(
				R.array.locations_list);

		for (int i = 0; i < localities.length; i++) {
			SelectPlaceNameModel sp = new SelectPlaceNameModel();
			sp.placeName = localities[i];
			selectPlaceNameList.add(sp);
			newArrayList.add(sp);
		}

	}*/
	//////////////////////////////////////////////////////////////////
	
	
	
	/**
	 * Method for setting location data of cities.............
	 * @param selectedPlace
	 * @param checkLocationAndTitle
	 */
	private void setLocationsData(String selectedPlace, boolean checkLocationAndTitle) {
		String[] locations;
			 String citiesFile = Logger.readAssetsCitiesFile(ctx, "cities.txt");
				 locations = citiesFile.split(",");
		String[] selectPlaceArr = selectedPlace.split(",");
		for (int i = 0; i < locations.length; i++) {
			SelectPlaceNameModel sp = new SelectPlaceNameModel();
			sp.placeName = locations[i];
			for (int j = 0; j < selectPlaceArr.length; j++) {
				if (sp.placeName.equalsIgnoreCase(selectPlaceArr[j])) {
					sp.check = true;
					break;
				}
			}
			selectPlaceNameList.add(sp);
			newArrayList.add(sp);
		}

	}

	/**
	 * Method to set selected locality..........
	 * @param placeName
	 */
	public void setSelectedLocality(String placeName) {
		d.dismiss();
		if (isCityOrLocality.equalsIgnoreCase(getResources().getString(
				R.string.locality)))
			localityTextView.setText(placeName);
		else
			cityTextView.setText(placeName);
	}

	/*
	public void performePodtAdLocality(View v)
	{
		String location = localityTextView.getText().toString();
		showLocationAndTitleDialog(location,true);
	}
	*/
	
///////////////////////////////////////////////////////////////	
	public void showLocationAndTitleDialog(String location, final boolean checkLocationAndTitle) {
		selectPlaceNameList = new ArrayList<SelectPlaceNameModel>();
		newArrayList = new ArrayList<SelectPlaceNameModel>();
	
		final Dialog dlg=new Dialog(ctx, R.style.FullHeightDialog);
		dlg.setContentView(R.layout.select_place_name);
		dlg.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		dlg.getWindow().setGravity(Gravity.CENTER);
		// adding text dynamically
		EditText placeNameEditText = (EditText) dlg
				.findViewById(R.id.placeNameEditText);
		ListView placeNameListView = (ListView) dlg
				.findViewById(R.id.placeNameListView);
		Button applyLocationBtn = (Button) dlg
				.findViewById(R.id.applyLocationBtn);
		// placeNameListView.setItemsCanFocus(false);
		if(location.equalsIgnoreCase(getResources().getString(R.string.select_locality))){
			placeNameEditText.setText("");
		} else {
			placeNameEditText.setText(location);
		}
		
		setLocationsData(location,checkLocationAndTitle);
		placeNameListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		placeNameListView.setTextFilterEnabled(true);
		// setLocationAdapter(selectPlaceNameList, placeNameListView);
		selectLocalLityAdapter = new SelectPlaceNameAdapter(ctx,
				cityList);
		//newArrayList
		placeNameListView.setAdapter(selectLocalLityAdapter);

		placeNameEditText.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				cityList.clear();
				for (int i = 0; i < selectPlaceNameList.size(); i++) {
					if ((selectPlaceNameList.get(i).placeName.toString()
							.toLowerCase()).indexOf(s.toString().toLowerCase()) > -1) {
						cityList.add(selectPlaceNameList.get(i));
					}
				}
				
				selectLocalLityAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		applyLocationBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dlg.dismiss();
				String selectedLocations = Util
						.getLocations(cityList);
				//selectPlaceNameList
				if(checkLocationAndTitle && !selectedLocations.equals("")){
					localityTextView.setText(selectedLocations);
				} else {
					localityTextView.setText("Select a Locality");
				}
				
			}
		});

		dlg.show();

		
	}

	public void setPostAdGoogleData() {

		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {

			if (Util.isNetworkAvailable(getApplicationContext())) {
				startBusy();
				if (mGoogleApiClient != null
						&& !mGoogleApiClient.isConnecting()) {
					mSignInClicked = true;
					resolveSignInError();
				}
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			postDatalist();
		}

	}

}