package com.nkaplication.adp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nkaplication.BaseActivity;
import com.nkaplication.R;
import com.nkaplication.model.MenuDrawerItem;
import com.nkaplication.prefs.MyPrefs;

public class MenuListAdapter extends BaseAdapter {

	private Context context;
	private int image = 0;
	private ArrayList<MenuDrawerItem> navDrawerItems;

	public MenuListAdapter(Context context,
			ArrayList<MenuDrawerItem> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.menu, null);
		}

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		TextView emailTextTitle = (TextView) convertView
				.findViewById(R.id.menuEmailId);
		imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		txtTitle.setText(navDrawerItems.get(position).getTitle());
		if(MyPrefs.getInstance().email.equals("")
				&& position== 4)
			
		{	imgIcon.setVisibility(View.GONE);
		    txtTitle.setVisibility(View.GONE);
		   
		   
			
		}
   if(!MyPrefs.getInstance().email.equals("")
			&& position== 4){
			
	    imgIcon.setVisibility(View.VISIBLE);
		txtTitle.setVisibility(View.VISIBLE);
		imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		txtTitle.setText(navDrawerItems.get(position).getTitle());
		
		
		}
			
		if (!MyPrefs.getInstance().email.equals("")
				&& navDrawerItems
						.get(position)
						.getTitle()
						.equalsIgnoreCase(
								context.getResources().getString(
										R.string.my_account)))
		{
			emailTextTitle.setVisibility(View.VISIBLE);
			emailTextTitle.setText(MyPrefs.getInstance().email);
		}
		image = position + 1;
		convertView.setTag("" + image);
		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				((BaseActivity) context).onClickButton(Integer.parseInt(v
						.getTag().toString()));
			}
		});

		return convertView;
	}

}
