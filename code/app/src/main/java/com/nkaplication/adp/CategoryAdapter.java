package com.nkaplication.adp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nkaplication.R;
import com.nkaplication.model.MenuDrawerItem;

public class CategoryAdapter extends BaseAdapter {

	private Context context;
	private int image = 0;
	private ArrayList<MenuDrawerItem> electronicDrawerItems;

	public CategoryAdapter(Context context,
			ArrayList<MenuDrawerItem> electronicDrawerItems) {
		this.context = context;
		this.electronicDrawerItems = electronicDrawerItems;
	}

	@Override
	public int getCount() {
		return electronicDrawerItems.size();
		// electronicDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return electronicDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.category_row, null);
		}

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		imgIcon.setImageResource(electronicDrawerItems.get(position).getIcon());
		txtTitle.setText(electronicDrawerItems.get(position).getTitle());
		image = position + 1;
		convertView.setTag("" + image);
		/*
		 * convertView.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { ((BaseActivity)
		 * context).onClickButton(Integer.parseInt(v .getTag().toString())); }
		 * });
		 */

		return convertView;
	}

}
