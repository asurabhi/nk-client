package com.nkaplication.adp;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.nkaplication.ItemDetail;

public class ViewPagerAdapter extends PagerAdapter {

	private Context activity;
	private ArrayList<String> images = new ArrayList<String>();

	public ViewPagerAdapter(Context act, ArrayList<String> images) {
		this.images = images;
		activity = act;
	}

	public int getCount() {
		return images.size();
	}

	public Object instantiateItem(View collection, final int position) {
		ImageView view = new ImageView(activity);
		view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT));
		if (!images.get(position).trim().equals(""))
			// view.setImageBitmap(Util.getBitmapOfImage(activity,
			// images.get(position)));
			view.setImageBitmap(BitmapFactory.decodeFile(images.get(position)));
		((ViewPager) collection).addView(view, 0);
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				((ItemDetail) activity).pagerClickAction(position);
			}
		});
		return view;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
}
