package com.nkaplication.adp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nkaplication.Home;
import com.nkaplication.MyAdsAndShortList;
import com.nkaplication.R;
import com.nkaplication.model.AdItem;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.Util;

public class MyAdsAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<AdItem> adsItems;
	private boolean checkEditDelete;

	public MyAdsAdapter(Context context, ArrayList<AdItem> adsItems2,
			boolean checkEditDelete) {
		this.context = context;
		this.adsItems = adsItems2;
		this.checkEditDelete = checkEditDelete;
	}

	@Override
	public int getCount() {
		return adsItems.size();
	}

	@Override
	public Object getItem(int position) {
		return adsItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.my_ads_row_list, null);
		}

		ImageView imgIcon = (ImageView) convertView
				.findViewById(R.id.myAdsRowImageIcon);
		TextView title = (TextView) convertView
				.findViewById(R.id.myAdsRowAdTitle);
		TextView address = (TextView) convertView
				.findViewById(R.id.myAdsRowAddress);
		TextView cost = (TextView) convertView
				.findViewById(R.id.myAdsRowAdCost);
		TextView condition = (TextView) convertView
				.findViewById(R.id.myAdsRowAdCondition);

		LinearLayout myAdsEditButton = (LinearLayout) convertView
				.findViewById(R.id.myAdsEditButton);
		LinearLayout myAdsDeleteButton = (LinearLayout) convertView
				.findViewById(R.id.myAdsDeleteButton);
		final AdItem item = adsItems.get(position);

		// new ImageLoadTask(item.imageIcon, imgIcon).execute();

		Bitmap image;
		try {
			// imgIcon.setImageBitmap(Util.getBitmapOfImage(context,
			// item.images.get(0)));
			imgIcon.setImageBitmap(BitmapFactory.decodeFile(item.images.get(0)));
			// System.out.println(image);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String titleStr = "";
		if (item.condition != null && !item.condition.equals(""))
			titleStr = item.title + " is " + item.condition;
		else
			titleStr = item.title;
		title.setText(titleStr);
		condition.setText(item.description);
		address.setText(item.city + "," + item.district);
		String price = item.lowPrice;
		if (item.highPrice != null && !item.highPrice.equals(""))
			price = item.lowPrice + "-" + item.highPrice;
		cost.setText(price);

		if (checkEditDelete) {
			myAdsEditButton.setVisibility(View.VISIBLE);
			myAdsDeleteButton.setVisibility(View.VISIBLE);
		} else {
			myAdsEditButton.setVisibility(View.GONE);
			myAdsDeleteButton.setVisibility(View.GONE);
		}

		myAdsEditButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (context != null && context instanceof MyAdsAndShortList) {

					((MyAdsAndShortList) context).moveOnEditMyAdsScreen(
							position, item);
				}

				if (context != null && context instanceof Home) {

					((Home) context).moveOnEditMyAdsScreen(position, item);
				}
			}
		});

		myAdsDeleteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// ((MyAdsAndShortList) context).deleteAds(position, item);

				if (context != null && context instanceof MyAdsAndShortList) {
					((MyAdsAndShortList) context).deleteAds(position, item);
				}

				if (context != null && context instanceof Home) {

					((Home) context).deleteAds(position, item);
				}

			}
		});

		return convertView;
	}

}
