package com.nkaplication.adp;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<String> {

	public SpinnerAdapter(Context context, String[] arr, int layout) {
		super(context, layout, arr);
	}

	/*
	 * public SpinnerAdapter(Context context,String[] arr,boolean b) {
	 * super(context, R.layout.spinner_text_view,arr); this.b=b; }
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		// ((TextView) v).setTextSize(13);
		/*
		 * if(!b) { ((TextView) v).setTextColor(Color.WHITE); }
		 */
		((TextView) v).setTextColor(Color.BLACK);

		return v;
	}

	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View v = super.getDropDownView(position, convertView, parent);

		((TextView) v).setGravity(Gravity.CENTER);

		return v;

	}

}
