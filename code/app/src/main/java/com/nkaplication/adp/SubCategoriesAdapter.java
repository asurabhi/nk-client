package com.nkaplication.adp;

import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nkaplication.R;
import com.nkaplication.SubCategories;
import com.nkaplication.model.AdItem;
import com.nkaplication.prefs.MyPrefs;

public class SubCategoriesAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<AdItem> electricMobilesItems;

	public SubCategoriesAdapter(Context context,
			ArrayList<AdItem> electricMobilesItems2) {
		this.context = context;
		this.electricMobilesItems = electricMobilesItems2;
	}

	@Override
	public int getCount() {
		return electricMobilesItems.size();
	}

	@Override
	public Object getItem(int position) {
		return electricMobilesItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.sub_categories_row, null);
		}

		ImageView imgIcon = (ImageView) convertView
				.findViewById(R.id.electronicMobileImageIcon);
		TextView title = (TextView) convertView
				.findViewById(R.id.electronicMobileTitle);
		TextView address = (TextView) convertView
				.findViewById(R.id.electronicMobileAddress);
		TextView cost = (TextView) convertView
				.findViewById(R.id.electronicMobileCost);
		TextView condition = (TextView) convertView
				.findViewById(R.id.electronicMobileCondition);

		ImageView favImageIcon = (ImageView) convertView
				.findViewById(R.id.electronicMobileFavoriteIcon);

		final AdItem item = electricMobilesItems.get(position);
		
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			favImageIcon.setImageResource(R.drawable.favorite_disable_icon);
		} else {
			if (item.isfavourite)
				favImageIcon.setImageResource(R.drawable.favorite_enable_icon);
			else
				favImageIcon.setImageResource(R.drawable.favorite_disable_icon);
		}
		
	
		Bitmap image;
		try {

			// imgIcon.setImageBitmap(Util.getBitmapOfImage(context,
			// item.images.get(0)));
			imgIcon.setImageBitmap(BitmapFactory.decodeFile(item.images.get(0)));
			// System.out.println(image);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		title.setText(item.title);
		condition.setText(item.description);
		address.setText(item.city + "," + item.district);
		String price = item.lowPrice;
		if (item.highPrice != null && !item.highPrice.equals(""))
			price = item.lowPrice + "-" + item.highPrice;
		cost.setText(price);
		
		
		
		favImageIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((SubCategories) context).likeUnlikeActionSubCategories(item,position);
			}
		});

		return convertView;
	}

}
