package com.nkaplication.adp;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nkaplication.R;
import com.nkaplication.model.SelectPlaceNameModel;

public class SelectPlaceNameAdapter extends ArrayAdapter<SelectPlaceNameModel>

{
	private ArrayList<SelectPlaceNameModel> selectPlacelist;
	private Context context;

	public SelectPlaceNameAdapter(Context context,
			ArrayList<SelectPlaceNameModel> selectPlaceNameList) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.select_place_name_row, selectPlaceNameList);
		this.context = context;
		this.selectPlacelist = selectPlaceNameList;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		LayoutInflater inflate = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View view = inflate.inflate(R.layout.select_place_name_row, parent,
				false);

		TextView placeNameTextView = (TextView) view
				.findViewById(R.id.placeNameTextView);
		final ImageView checkImage = (ImageView) view
				.findViewById(R.id.checkImage);
		checkImage.setVisibility(View.VISIBLE);

		// CheckBox checkLoc = (CheckBox) view.findViewById(R.id.checkLoc);

		final SelectPlaceNameModel selectPlaceNameModel = selectPlacelist
				.get(position);
		placeNameTextView.setText(selectPlaceNameModel.placeName);

		if (selectPlaceNameModel.check) {
			checkImage.setImageResource(R.drawable.check);
		} else {
			checkImage.setImageResource(R.drawable.uncheck);
		}

		// checkLoc.setChecked(selectPlaceNameModel.check);
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (selectPlaceNameModel.check) {
					checkImage.setImageResource(R.drawable.uncheck);
					selectPlaceNameModel.check = false;
				} else {
					checkImage.setImageResource(R.drawable.check);
					selectPlaceNameModel.check = true;
				}

			}
		});

		return view;
	}

}
