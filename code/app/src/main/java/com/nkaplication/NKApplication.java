package com.nkaplication;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.nkaplication.model.AdItem;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.NKLocationListener;

/**
 * @author Brijesh dutt
 * 
 * 
 *         this singleton class.
 * 
 */
public class NKApplication extends Application {

	private static NKApplication _instance;
	public AdItem selectedItem;
	public String selectedCategory;
	public String selectedSubCategory;
	
	private NKLocationListener nll;

	LocationManager locMan;
	public Location loc;
	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		_instance = this;
		locMan = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		
		//requestLocation();
		MyPrefs.getInstance().loadPrefs(this);
	}

	public static NKApplication getInstance() {
		return _instance;
	}
	
	/**
	 * Method to check GPS is enabled or not...........
	 * @return
	 */
	public boolean isGPSEnabled(){
		return locMan.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
	
	/**
	 * Method for requesting locations...........
	 */
	

	public void requestLocation(NKLocationListener ll){
		this.nll=ll;
		//locMan.removeUpdates(gpsListenerGPS);
	locMan.removeUpdates(gpsListenerNetwork);
		/*locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 01,
				gpsListenerGPS);*/
		locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100,
				01, gpsListenerNetwork);
		
	}
	

	LocationListener gpsListenerNetwork = new LocationListener() {

		public void onStatusChanged(String provider, int status, Bundle extras) {
			System.out.println("provider status changed  " + status);
		}

		public void onProviderEnabled(String provider) {

		}

		public void onProviderDisabled(String provider) {
			System.out.println("provider disabled ");
		}

		public void onLocationChanged(Location location) {
		//	locMan.removeUpdates(gpsListenerGPS);
			locMan.removeUpdates(gpsListenerNetwork);
			loc = location;
			if(nll != null){
				nll.onLocation(location);
			}

		}

	};
}
