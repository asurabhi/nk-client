package com.nkaplication;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nkaplication.adp.LocalitiesAdapter;
import com.nkaplication.adp.MyAdsAdapter;
import com.nkaplication.db.NKDB;
import com.nkaplication.model.AdItem;
import com.nkaplication.model.MenuDrawerItem;
import com.nkaplication.model.SelectPlaceNameModel;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Logger;
import com.nkaplication.util.NKLocationListener;
import com.nkaplication.util.Util;

/**
 * @author Brijesh dutt
 * 
 *         Home Screen of application.
 */
public class Home extends BaseActivity implements Constants {

	private DrawerLayout drawer_layout;
	private ListView lv, homeAdsList;
	//private Spinner citySpin;
	private ArrayList<String> allAds = new ArrayList<String>();
	private MyAdsAdapter adapter;
	private EditText homeSearch;
	private ArrayList<AdItem> searchedAdsList = new ArrayList<AdItem>();
	private ArrayList<AdItem> allAdsList = new ArrayList<AdItem>();
	private LinearLayout homeCategoriesLayout,homeSpinnerLayoutId;
	private String[] categoryTitle;
	private TypedArray categoryIcons;
	private ArrayList<MenuDrawerItem> categories;
	private ArrayList<View> views = new ArrayList<View>();
	private MenuDrawerItem item;
	private TextView text,homeCity;
	private ArrayList<SelectPlaceNameModel> selectCityNameList = new ArrayList<SelectPlaceNameModel>();
	private ArrayList<SelectPlaceNameModel> cityList = new ArrayList<SelectPlaceNameModel>();
	private Dialog d;
	private LocalitiesAdapter selectPlaceAdapter;
	private NKDB db;
	/**
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.home_correct);



		homeAdsList = (ListView) findViewById(R.id.homeAdsList);
		lv = (ListView) findViewById(R.id.menuList);
		drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
		homeCity = (TextView) findViewById(R.id.homeCityId);
		menuListButton = (ImageView) findViewById(R.id.menuListButton);
		homeSearch = (EditText) findViewById(R.id.homeSearch);
		homeCategoriesLayout = (LinearLayout) findViewById(R.id.homeCategoriesLayout);
		// initActionBar(drawer_layout, lv);
		String[] city = getResources().getStringArray(R.array.city);
		/*SpinnerAdapter cityAdp = new SpinnerAdapter(this, city,
				R.layout.home_spinner_text_view);
		cityAdp.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		
		citySpin.setAdapter(cityAdp);*/
		if(homeCity.getText().toString().equals(""))
		{
		NKApplication.getInstance().requestLocation(new NKLocationListener() {
			
			@Override
			public void onLocation(Location c) {
				
				if( homeCity.getText().toString().equals(""))
				{
					if(c != null){
						boolean isExist = setCityListData(Util.getCity(c.getLatitude(), c.getLongitude()));
						if (isExist  )
							homeCity.setText(Util.getCity(c.getLatitude(), c.getLongitude()));
						else
						{
							
							homeCity.setText(Util.getCity(c.getLatitude(), c.getLongitude()));
						}
						} else {
							
							Util.showToastMessage(Home.this, getResources().getString(R.string.location_not_found_text));
							if(!setCityListData(Util.getCity(24.33, 77.369)))
									homeCity.setText(cityList.get(0).placeName);
							else
								homeCity.setText(Util.getCity(24.33, 77.369));
						}
						
				}
				
			}
		});
		
		
		boolean isEx = setCityListData(Util.getCity(24.33, 77.369));
		}
		homeSpinnerLayoutId=(LinearLayout)findViewById(R.id.homeSpinnerLayoutId);
		
		
		
		
		homeSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				
				searchedAdsList.clear();
				for (int i = 0; i < allAdsList.size(); i++) {
					AdItem ad = allAdsList.get(i);
					/*if ((ad.description != null && ad.description.toLowerCase().contains(s.toString().toLowerCase()))
							|| (ad.title != null && ad.title.toLowerCase().contains(s.toString().toLowerCase())))*/
					
					if ((ad.description != null && ad.description.toLowerCase()
							.indexOf(s.toString().toLowerCase()) > -1)
							|| (ad.title != null && ad.title.toLowerCase()
									.indexOf(s.toString().toLowerCase()) > -1)){
						searchedAdsList.add(allAdsList.get(i));
						setDataIntoListView();
						
					}
				}

				if (s.length() == 0) {
					if (adapter != null)
						adapter.notifyDataSetChanged();
					if (homeAdsList != null)
						homeAdsList.setVisibility(View.GONE);
					Util.hideSoftKeyboard(Home.this);
				} else {
                 	homeAdsList.setVisibility(View.VISIBLE);
					setDataIntoListView();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		homeSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				drawer_layout.closeDrawer(lv);
			}
		});

	/*	if (getIntent().getBooleanExtra("extra", false))
			finish();
*/
		getCategoriesDataInList();

		inflateCategoryRowOnLayout();
		//startBusy("Waiting for GPS...");
		
	
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		db = new NKDB(Home.this);
		allAds = db.getAllFiles();
		parseData();
		setHeaderWithSlider();
	}
	
	
	
	
	/**
	 * Method for inflating category list in home screen........... 
	 */
	private void inflateCategoryRowOnLayout() {
		LayoutInflater layoutInflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = null;

		for (int i = 0; i < categories.size(); i++) {
			if (i == 0 || i % 2 == 0) {
				v = layoutInflater.inflate(R.layout.home_row, null);
				item = categories.get(i);
				ImageView imageIcon = (ImageView) v
						.findViewById(R.id.homeCategoryIconFirst);
				LinearLayout layout = (LinearLayout) v
						.findViewById(R.id.homeCategoryFirst);
				text = (TextView) v.findViewById(R.id.homeCategoryTextFirst);
				text.setText(item.getTitle());
				layout.setTag("" + i);
				imageIcon.setImageResource(item.getIcon());

				layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (categories
								.get(Integer.parseInt(v.getTag().toString()))
								.getTitle()
								.equalsIgnoreCase(
										getResources().getString(
												R.string.electronics))) {
							NKApplication.getInstance().selectedCategory = categories
									.get(Integer
											.parseInt(v.getTag().toString()))
									.getTitle();
							Intent intent = new Intent(Home.this,
									Category.class);
							startActivity(intent);
						} else {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.not_implemented_yet),
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				homeCategoriesLayout.addView(v);
			}
			if (i % 2 == 1 && v != null) {

				item = categories.get(i);
				ImageView imageIcon = (ImageView) v
						.findViewById(R.id.homeCategoryIconSecond);
				text = (TextView) v.findViewById(R.id.homeCategoryTextSecond);
				LinearLayout layout = (LinearLayout) v
						.findViewById(R.id.homeCategorySecond);
				text.setText(item.getTitle());
				layout.setTag("" + i);
				imageIcon.setImageResource(item.getIcon());
				layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (categories
								.get(Integer.parseInt(v.getTag().toString()))
								.getTitle().equalsIgnoreCase(KEY_ELECTRONICS)) {
							NKApplication.getInstance().selectedCategory = categories
									.get(Integer
											.parseInt(v.getTag().toString()))
									.getTitle();
							Intent intent = new Intent(Home.this,
									Category.class);
							startActivity(intent);
						} else {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.not_implemented_yet),
									Toast.LENGTH_SHORT).show();
						}
					}
				});

			}
		}

	}

	/**
	 * method of menu slider
	 */

	private void setHeaderWithSlider() {

		initActionBar(drawer_layout, lv);
		drawer_layout.setDrawerListener(this);
	}

	/**
	 * Start an activity of Electronics
	 */

	/**
	 * Start an activity of PostAds
	 */

	public void performePostAds(View v) {
		Intent intent = new Intent(Home.this, PostAd.class);
		startActivity(intent);
	}

	/**
	 * Method for parsing ads list data.........
	 */
	private void parseData() {
		try {
			allAdsList.clear();
			for (int i = 0; i < allAds.size(); i++) {
                String data = Logger.readLogFile(Home.this, allAds.get(i));
				JSONObject obj = new JSONObject(data);
				AdItem allItem = new AdItem();
				allItem.populateFromJson(obj);
				allAdsList.add(allItem);
			}
			if (adapter != null)
				adapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method for set ads list data into listview...........
	 */
	private void setDataIntoListView() {
		boolean checkEditDelete = false;
		adapter = new MyAdsAdapter(Home.this, searchedAdsList,checkEditDelete);
		homeAdsList.setAdapter(adapter);
		
		//homeAdsList
		homeAdsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				
				
				NKApplication.getInstance().selectedItem = searchedAdsList
						.get(position);
				String path = db.getPathByAdId(NKApplication.getInstance().selectedItem.adItemId);
				Logger.logFile = new File(path);
				Intent intent = new Intent(Home.this, ItemDetail.class);
				startActivity(intent);
				
			//	int i =searchedAdsList.get(position).adItemId;
          //	NKApplication.getInstance().selectedItem =allAdsList.get(i-1);
			}
		});
	}

	/**
	 * Method for getting category list and images.........
	 */
	private void getCategoriesDataInList() {
		categoryTitle = getResources().getStringArray(R.array.categories_text);

		// nav drawer icons from resources
		categoryIcons = getResources().obtainTypedArray(
				R.array.categories_icons);

		categories = new ArrayList<MenuDrawerItem>();

		// adding nav drawer items to array
		// Home
		for (int i = 0; i < categoryTitle.length-1; i++) {
			categories.add(new MenuDrawerItem(categoryTitle[i], categoryIcons
					.getResourceId(i, -1)));
		}
		
		
		
		
		
		

	}
	
	
	
	/**
	 * Method click on select city layout.......
	 * @param v
	 */
	public void onClickCityLayout(View v)
	{
		showLocationDialog(cityList, selectCityNameList);
	}
	
	
	
	/**
	 * Method for showing location selection dialog.........
	 * @param list
	 * @param selectedList
	 */
	public void showLocationDialog(final ArrayList<SelectPlaceNameModel> list,
			final ArrayList<SelectPlaceNameModel> selectedList) {
		d = new Dialog(this, R.style.FullHeightDialog);
		d.setContentView(R.layout.select_place_name);
		d.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		d.getWindow().setGravity(Gravity.CENTER);

		// adding text dynamically
		EditText placeNameEditText = (EditText) d
				.findViewById(R.id.placeNameEditText);
		ListView placeNameListView = (ListView) d
				.findViewById(R.id.placeNameListView);
		placeNameListView.setDivider(getResources().getDrawable(
				R.drawable.devider));
		Button applyLocationBtn = (Button) d
				.findViewById(R.id.applyLocationBtn);
		applyLocationBtn.setVisibility(View.GONE);
		// placeNameListView.setItemsCanFocus(false);
		placeNameListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		placeNameListView.setTextFilterEnabled(true);
		// setLocationAdapter(selectPlaceNameList, placeNameListView);
		selectPlaceAdapter = new LocalitiesAdapter(this, list);
		placeNameListView.setAdapter(selectPlaceAdapter);

		placeNameEditText.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				list.clear();
				for (int i = 0; i < selectedList.size(); i++) {
					if ((selectedList.get(i).placeName.toString().toLowerCase())
							.indexOf(s.toString().toLowerCase()) > -1) {
						list.add(selectedList.get(i));
					}
				}
				selectPlaceAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		d.show();

	}

	
	/**
	 * Method for set city list data.........
	 * @param city
	 * @return
	 */
	private boolean setCityListData(String city) {

		boolean isExist = false;
		ctx=this;
		String citiesFile = Logger.readAssetsCitiesFile(ctx, "cities.txt");
		String[] cities = citiesFile.split(",");

		for (int i = 0; i < cities.length; i++) {
			if (city.equalsIgnoreCase(cities[i])) {
				isExist = true;
			} else {
				SelectPlaceNameModel sp = new SelectPlaceNameModel();
				sp.placeName = cities[i];
				selectCityNameList.add(sp);
				cityList.add(sp);
			}
		}
		return isExist;
		//isExist

	}
	
	
	/**
	 * Method for select city........
	 * @param placeName
	 */
	public void setSelectedCity(String placeName) {
		d.dismiss();
		/*if (isCityOrLocality.equalsIgnoreCase(getResources().getString(
				R.string.locality)))
			localityTextView.setText(placeName);
		else*/
			homeCity.setText(placeName);
	}
	
	
	
	/**
	 * Method for edit ad item detail..........
	 * @param position
	 * @param item
	 */
	public void moveOnEditMyAdsScreen(int position, AdItem item) {
		NKApplication.getInstance().selectedItem = item;
		String path = db.getPathByAdId(NKApplication.getInstance().selectedItem.adItemId);
		Logger.logFile = new File(path);
		//Logger.logFile = new File(allAds.get(position));
		Intent intent = new Intent(Home.this, EditAd.class);
		startActivity(intent);
	}

	/**
	 * Method for deletion of ad item.............
	 * @param position
	 * @param item
	 */
	public void deleteAds(int position, AdItem item) {
		startBusy();
		String path = db.getPathByAdId(item.adItemId);
		db.deleteFileRow(path);
		//db.deleteFileRow(allAds.get(position));
		searchedAdsList.remove(item);
		adapter.notifyDataSetChanged();
		stopBusy();
	}
	
	/**
	 * Method perform action click on search button.............
	 * @param v
	 */
	public void performSearchAction(View v){
		Util.hideSoftKeyboard(Home.this);
	}
	
	/*
	 * public void performeElectronics(View v) {
	 * NKApplication.getInstance().selectedCategory = KEY_ELECTRONICS; Intent
	 * intent = new Intent(Home.this, Category.class); startActivity(intent); }
	 */
}