package com.nkaplication.fragment;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.nkaplication.R;
import com.nkaplication.SubCategories;
import com.nkaplication.adp.SelectPlaceNameAdapter;
import com.nkaplication.model.SelectPlaceNameModel;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Logger;
import com.nkaplication.util.Util;
import com.rangeseekbar.RangeSeekBar;
import com.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener;

public class FilterFragment extends Fragment implements Constants {

	private Button applyFilterButton, cancelFilterButton;
	private ImageView editPriceRangeFilterButton;
	private TextView priceRangeTextView, locationTextView;
	// private SeekBar seekBarPriceRange;
	private EditText minValueEditText, maxValueEditText,titleEditText;
	private RadioButton offeringRadioBtn, wantedRadioBtn;
	private RangeSeekBar<Integer> rangeSeekBar;
	private ArrayList<SelectPlaceNameModel> selectPlaceNameList;
	private ArrayList<SelectPlaceNameModel> newArrayList;
	private SelectPlaceNameAdapter selectPlaceAdapter;
	private LinearLayout locationLinearLayout;
	protected boolean isClicked;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.filter_dialog_box, container, false);
		priceRangeTextView = (TextView) v.findViewById(R.id.priceRangeTextView);
		locationTextView = (TextView) v.findViewById(R.id.locationTextView);
		editPriceRangeFilterButton = (ImageView) v
				.findViewById(R.id.editPriceRangeFilterButton);
		offeringRadioBtn = (RadioButton) v.findViewById(R.id.offeringRadioBtn);
		wantedRadioBtn = (RadioButton) v.findViewById(R.id.wantedRadioBtn);
		applyFilterButton = (Button) v.findViewById(R.id.applyFilterButton);
		cancelFilterButton = (Button) v.findViewById(R.id.cancelFilterButton);
		locationLinearLayout = (LinearLayout) v
				.findViewById(R.id.locationLinearLayout);

		titleEditText = (EditText) v.findViewById(R.id.titleEditText);
		
		LinearLayout seekbarLayout = (LinearLayout) v
				.findViewById(R.id.seekbarLayout);
		minValueEditText = (EditText) v.findViewById(R.id.minValueEditText);
		maxValueEditText = (EditText) v.findViewById(R.id.maxValueEditText);

		applyFilterButton.setOnClickListener(listener);
		cancelFilterButton.setOnClickListener(listener);
		locationLinearLayout.setOnClickListener(listener);
		editPriceRangeFilterButton.setOnClickListener(listener);

		Bundle args = getArguments();
		String loc = args.getString("location");
		String range = args.getString("range");
		String adtype = args.getString("adType");
		String title = args.getString("title");

		locationTextView.setText(loc);
		titleEditText.setText(title);
		priceRangeTextView.setText(range);
		rangeSeekBar = new RangeSeekBar<Integer>(getActivity());
		// Set the range
		rangeSeekBar.setRangeValues(0, 100);
		
		if(!range.equalsIgnoreCase("")){
			String[] rangeArr = range.split(" ");
			minValueEditText.setText(rangeArr[0]);
			maxValueEditText.setText(rangeArr[2]);
			
			rangeSeekBar.setSelectedMinValue(Integer.parseInt(rangeArr[0])/500);
			rangeSeekBar.setSelectedMaxValue(Integer.parseInt(rangeArr[2])/500);
		}
		
		// seekBarPriceRange.setProgress(Integer.parseInt(range)/500);
		if (adtype.equalsIgnoreCase(KEY_SELL)) {
			offeringRadioBtn.setChecked(true);
		} else {
			wantedRadioBtn.setChecked(true);
		}

		
		

		// Add to layout
		seekbarLayout.addView(rangeSeekBar);

		rangeSeekBar
				.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
					@Override
					public void onRangeSeekBarValuesChanged(
							RangeSeekBar<?> bar, Integer minValue,
							Integer maxValue) {
						priceRangeTextView.setText("" + (minValue * 500)
								+ " to " + (maxValue * 500));
						minValueEditText.setText("" + (minValue * 500));
						maxValueEditText.setText("" + (maxValue * 500));

					}

					@Override
					public void onRangeSeekBarProgressChanged(
							RangeSeekBar<?> bar, Integer minValue,
							Integer maxValue) {
						priceRangeTextView.setText("" + (minValue * 500)
								+ " to " + (maxValue * 500));
						if (isClicked) {
							isClicked = false;
							minValueEditText.setText("" + (minValue * 500));
							maxValueEditText.setText("" + (maxValue * 500));
						}
					}

				});

		return v;
	}

	private OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == applyFilterButton) {
				PerformApplyAction();
			} else if (v == cancelFilterButton) {
				PerformCancelAction();
			} else if (v == locationLinearLayout) {
				String location = locationTextView.getText().toString();
				showLocationAndTitleDialog(location);
			} else if (v == editPriceRangeFilterButton) {
				performEditPriceRangeAction();
			}
		}
	};

	/**
	 * Method perform action click on apply button in filter dialog..........
	 */
	private void PerformApplyAction() {

		String range = priceRangeTextView.getText().toString();
		String location = locationTextView.getText().toString();
		String title = titleEditText.getEditableText().toString();

		String adType = KEY_SELL;
		if (wantedRadioBtn.isChecked()) {
			adType = KEY_BUY;
		}

		/*
		 * if (range.equalsIgnoreCase("0 to 50000")) {
		 * Util.showToastMessage(getActivity(),
		 * "Range should be greater than 0"); return; }
		 */
		((SubCategories) getActivity()).resetFilterLayout(location, range,
				adType, title);
	}

	/**
	 * Method perform action click on cancel button in filter dialog..........
	 */
	private void PerformCancelAction() {
		((SubCategories) getActivity()).performFilterAction(null);
	}

	/**
	 * Method perform to open dialog and select location and title parameters.........
	 * @param location
	 * @param checkLocationAndTitle
	 */
	public void showLocationAndTitleDialog(String location) {
		selectPlaceNameList = new ArrayList<SelectPlaceNameModel>();
		newArrayList = new ArrayList<SelectPlaceNameModel>();
		// final Dialog d = new Dialog(getActivity());
		// d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		// LayoutInflater inflater=LayoutInflater.from(getActivity());
		// View contentView=inflater.inflate(R.layout.instructions_popup_layout,
		// null);
		final Dialog d = new Dialog(getActivity(), R.style.FullHeightDialog);
		d.setContentView(R.layout.select_place_name);
		d.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		d.getWindow().setGravity(Gravity.CENTER);
		// adding text dynamically
		EditText placeNameEditText = (EditText) d
				.findViewById(R.id.placeNameEditText);
		ListView placeNameListView = (ListView) d
				.findViewById(R.id.placeNameListView);
		Button applyLocationBtn = (Button) d
				.findViewById(R.id.applyLocationBtn);
		// placeNameListView.setItemsCanFocus(false);
		placeNameEditText.setText(location);
		setLocationsData(location);
		placeNameListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		placeNameListView.setTextFilterEnabled(true);
		// setLocationAdapter(selectPlaceNameList, placeNameListView);
		selectPlaceAdapter = new SelectPlaceNameAdapter(getActivity(),
				newArrayList);
		placeNameListView.setAdapter(selectPlaceAdapter);

		placeNameEditText.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				newArrayList.clear();
				for (int i = 0; i < selectPlaceNameList.size(); i++) {
					if ((selectPlaceNameList.get(i).placeName.toString()
							.toLowerCase()).indexOf(s.toString().toLowerCase()) > -1) {
						newArrayList.add(selectPlaceNameList.get(i));
					}
				}

				selectPlaceAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		applyLocationBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.dismiss();
				String selectedLocations = Util
						.getLocations(selectPlaceNameList);
			    locationTextView.setText(selectedLocations);
			}
		});

		d.show();

	}

	/**
	 * Method for set selected location and title field data......
	 * @param selectedPlace
	 * @param checkLocationAndTitle
	 */
	private void setLocationsData(String selectedPlace) {
	
		String citiesFile = Logger.readAssetsCitiesFile(getActivity(), "cities.txt");
		String[] locations = citiesFile.split(",");
		
		String[] selectPlaceArr = selectedPlace.split(",");
		for (int i = 0; i < locations.length; i++) {
			SelectPlaceNameModel sp = new SelectPlaceNameModel();
			sp.placeName = locations[i];
			for (int j = 0; j < selectPlaceArr.length; j++) {
				if (sp.placeName.equalsIgnoreCase(selectPlaceArr[j])) {
					sp.check = true;
					break;
				}
			}
			selectPlaceNameList.add(sp);
			newArrayList.add(sp);
		}
	}

	/**
	 * Method for edit price range field min and max value......
	 */
	private void performEditPriceRangeAction() {

		String minValue = minValueEditText.getEditableText().toString();
		String maxValue = maxValueEditText.getEditableText().toString();

		if (minValue.equalsIgnoreCase("") || maxValue.equalsIgnoreCase("")) {
			Toast.makeText(getActivity(), "fields are not blank",
					Toast.LENGTH_SHORT).show();
			return;
		}

		rangeSeekBar.setSelectedMinValue(Integer.parseInt(minValue) / 500);
		rangeSeekBar.setSelectedMaxValue(Integer.parseInt(maxValue) / 500);
		isClicked = true;

	}
}
