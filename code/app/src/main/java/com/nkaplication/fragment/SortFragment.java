package com.nkaplication.fragment;

import com.nkaplication.SubCategories;
import com.nkaplication.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;

public class SortFragment extends Fragment {
	private RadioButton mostRecentRadioBtn, lowPriceRadioBtn, highPriceRadioBtn,
			pictureRadioBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sort_dialog_box, container, false);
		mostRecentRadioBtn = (RadioButton) v
				.findViewById(R.id.mostRecentRadioBtn);
		lowPriceRadioBtn = (RadioButton) v.findViewById(R.id.lowPriceRadioBtn);
		highPriceRadioBtn = (RadioButton) v
				.findViewById(R.id.highPriceRadioBtn);
		pictureRadioBtn = (RadioButton) v.findViewById(R.id.pictureRadioBtn);

		mostRecentRadioBtn.setOnClickListener(listener);
		lowPriceRadioBtn.setOnClickListener(listener);
		highPriceRadioBtn.setOnClickListener(listener);
		pictureRadioBtn.setOnClickListener(listener);

		Bundle args = getArguments();
		String sortType = args.getString("sortType");

		if (sortType.equalsIgnoreCase(getResources().getString(R.string.most_recent))) {
			mostRecentRadioBtn.setChecked(true);
		} else if (sortType.equalsIgnoreCase(getResources().getString(R.string.low_price))) {
			lowPriceRadioBtn.setChecked(true);
		} else if (sortType.equalsIgnoreCase(getResources().getString(R.string.high_price))) {
			highPriceRadioBtn.setChecked(true);
		} else if (sortType.equalsIgnoreCase(getResources().getString(R.string.with_picture))) {
			pictureRadioBtn.setChecked(true);
		}

		return v;
	}

	private OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v == mostRecentRadioBtn) {
				((SubCategories) getActivity())
						.resetSortLayout(getResources().getString(R.string.most_recent));
			} else if (v == lowPriceRadioBtn) {
				((SubCategories) getActivity())
						.resetSortLayout(getResources().getString(R.string.low_price));
			} else if (v == highPriceRadioBtn) {
				((SubCategories) getActivity())
						.resetSortLayout(getResources().getString(R.string.high_price));
			} else if (v == pictureRadioBtn) {
				((SubCategories) getActivity())
						.resetSortLayout(getResources().getString(R.string.with_picture));
			}
		}
	};
}
