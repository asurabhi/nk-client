package com.nkaplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class MaBaseSQLite extends SQLiteOpenHelper {

	public static final String TABLE1 = "file_mapping";
	public static final String AUTO_ID = "id";
	public static final String FILE_PATH = "filePath";
	public static final String EMAIL_ID = "emailId";
	public static final String CATEGORY = "category";
	public static final String SUB_CATEGORY = "subCategory";
	private static final String CREATE_TABLE1 = "create table " + TABLE1 + " "
			+ "(" + AUTO_ID + " integer primary key autoincrement, " + EMAIL_ID
			+ " text not null, " + CATEGORY + " text not null, " + SUB_CATEGORY
			+ " text not null, " + FILE_PATH + " text not null );";

	public MaBaseSQLite(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_TABLE1);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE " + TABLE1 + ";");
		onCreate(db);
	}

}