package com.nkaplication.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class NKDB {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "nk.db";

	private SQLiteDatabase bdd;

	private MaBaseSQLite maBaseSQLite;

	public NKDB(Context context) {
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}

	public void open() {
		bdd = maBaseSQLite.getWritableDatabase();
	}

	public void close() {
		bdd.close();
	}

	public SQLiteDatabase getBDD() {
		return bdd;
	}

	/**
	 * Method for inserting profile path data with parameters..........
	 * @param email
	 * @param path
	 * @param category
	 * @param subCategory
	 */
	public void insertProfilePath(String email, String path, String category,
			String subCategory) {
		ContentValues values = new ContentValues();
		values.put(MaBaseSQLite.EMAIL_ID, email);
		values.put(MaBaseSQLite.CATEGORY, category);
		values.put(MaBaseSQLite.SUB_CATEGORY, subCategory);
		values.put(MaBaseSQLite.FILE_PATH, path);
		open();
		bdd.insert(MaBaseSQLite.TABLE1, null, values);
		close();
	}

	/**
	 * Method for getting all Ads files..........
	 * @return
	 */
	public ArrayList<String> getAllFiles() {

		ArrayList<String> filePaths = new ArrayList<String>();
		Cursor c = null;
		try {

			open();
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.FILE_PATH }, null/*
																 * MaBaseSQLite.
																 * URL_COL
																 * +"='"+url+"'"
																 */, null,
					null, null, null);
			while (c.moveToNext()) {
				
				String filePath = c.getString(0);
				filePaths.add(filePath);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return filePaths;
	}

	/**
	 * Method of getting Ads files by email...........
	 * @param email
	 * @return
	 */
	public ArrayList<String> getMyAdsFilesByEmail(String email) {

		ArrayList<String> filePaths = new ArrayList<String>();
		Cursor c = null;
		try {

			open();
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.FILE_PATH },
					MaBaseSQLite.EMAIL_ID + "='" + email + "'", null, null,
					null, null);
			while (c.moveToNext()) {
				String filePath = c.getString(0);
				filePaths.add(filePath);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return filePaths;
	}

	/**
	 * Method for getting  Ads file from subcategory of specific category........
	 * @param category
	 * @param subcategory
	 * @return
	 */
	public ArrayList<String> getAdsFilesByCatSubCat(String category,
			String subcategory) {

		ArrayList<String> filePaths = new ArrayList<String>();
		Cursor c = null;
		try {

			open();

			String conditon = MaBaseSQLite.CATEGORY + "='" + category
					+ "' AND " + MaBaseSQLite.SUB_CATEGORY + "='" + subcategory
					+ "'";
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.FILE_PATH }, conditon, null,
					null, null, null);
			while (c.moveToNext()) {
				String filePath = c.getString(0);
				filePaths.add(filePath);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return filePaths;
	}
	
	/**
	 * Method for getting Ads files by category ..............
	 * @param category
	 * @return
	 */
	public ArrayList<String> getAdsFilesByCategory(String category) {

		ArrayList<String> filePaths = new ArrayList<String>();
		Cursor c = null;
		try {

			open();

			String conditon = MaBaseSQLite.CATEGORY + "='" + category
					+ "'";
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.FILE_PATH }, conditon, null,
					null, null, null);
			while (c.moveToNext()) {
				String filePath = c.getString(0);
				filePaths.add(filePath);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return filePaths;
	}

	/**
	 * Method for delete file from database by file name........
	 * @param filename
	 */
	public void deleteFileRow(String filename) {
		open();
		String conditon = MaBaseSQLite.FILE_PATH + "='" + filename
				+ "'";
		bdd.delete(MaBaseSQLite.TABLE1, conditon, null);
		close();
	}

	/**
	 * Method for get Ad id by file path from database........ 
	 * @param path
	 * @return
	 */
	public int getIdByFilePath(String path) {

		int adsId = 0;
		Cursor c = null;
		try {
         	open();
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.AUTO_ID },
					MaBaseSQLite.FILE_PATH + "='" + path + "'", null, null,
					null, null);
			while (c.moveToNext()) {
				adsId = c.getInt(0);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return adsId;
	}
	
	
	/**
	 * Method for get file path by Ad id from database.......
	 * @param adId
	 * @return
	 */
	public String getPathByAdId(int adId) {

		String path = null;
		Cursor c = null;
		try {
         	open();
			c = bdd.query(MaBaseSQLite.TABLE1,
					new String[] { MaBaseSQLite.FILE_PATH },
					MaBaseSQLite.AUTO_ID + "='" + adId + "'", null, null,
					null, null);
			while (c.moveToNext()) {
				path = c.getString(0);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
			close();
		}
		return path;
	}
}