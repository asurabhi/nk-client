package com.nkaplication;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nkaplication.model.AdItem;
import com.nkaplication.model.BaseModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Util;

/**
 * @author neelam goyal
 * 
 *         Show all detail about selected MyAds and we can update all
 *         information
 * 
 */
public class EditAd extends BaseActivity implements DataListener, Constants {

	// ArrayList<EditAdsMyRowModel> adsItems = new
	// ArrayList<EditAdsMyRowModel>();
	private LinearLayout mobilesImagesLayout, editMyAdsSellLayout,
			editMyAdsBuyLayout;
	private AdItem adsItem;
	private EditText editMyAdsTitle, editMyAdsDescription, editMyAdsSellPrice,
			editMyAdsBuyLowPrice, editMyAdsBuyHighPrice;
	private Bitmap thumbnail;
	private ImageButton addPhotoImageCapture;
	private int imageArraySize;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_my_ads);
		TextView headerTitle = (TextView) findViewById(R.id.headerTitle);
		headerTitle.setText(getResources().getString(R.string.edit_my_ads));
		mobilesImagesLayout = (LinearLayout) findViewById(R.id.mobilesImagesLayout);
		editMyAdsSellLayout = (LinearLayout) findViewById(R.id.editMyAdsSellLayout);
		editMyAdsBuyLayout = (LinearLayout) findViewById(R.id.editMyAdsBuyLayout);
		editMyAdsTitle = (EditText) findViewById(R.id.editMyAdsTitle);
		editMyAdsDescription = (EditText) findViewById(R.id.editMyAdsDescription);
		editMyAdsSellPrice = (EditText) findViewById(R.id.editMyAdsSellPrice);
		addPhotoImageCapture = (ImageButton) findViewById(R.id.addPhotoImageCapture);
		editMyAdsBuyLowPrice = (EditText) findViewById(R.id.editMyAdsBuyLowPrice);
		editMyAdsBuyHighPrice = (EditText) findViewById(R.id.editMyAdsBuyHighPrice);

		adsItem = NKApplication.getInstance().selectedItem;
		if (adsItem.status != null && adsItem.status.equalsIgnoreCase(KEY_SELL)) {
			editMyAdsSellPrice.setText(adsItem.lowPrice);
			editMyAdsBuyLayout.setVisibility(View.GONE);
		} else {
			editMyAdsBuyLowPrice.setText(adsItem.lowPrice);
			if (adsItem.highPrice != null && !adsItem.highPrice.equals(""))
				editMyAdsBuyHighPrice.setText(adsItem.highPrice);
			else
				editMyAdsBuyHighPrice.setVisibility(View.INVISIBLE);
			editMyAdsSellLayout.setVisibility(View.GONE);
		}
		editMyAdsTitle.setText(adsItem.title);
		editMyAdsDescription.setText(adsItem.description);
		imageArraySize = adsItem.images.size();
		showMoreImageWithInflate();
		/** Load data about Selected Ads Images */
		// EditAdsMyRowModel.load(this, this);

	}

	/**
	 * Method for showing more than one images after inflate.......
	 */
	private void showMoreImageWithInflate() {
		inflateImagesOnLayout();
		if (adsItem.images.size() < 4)
			addPhotoImageCapture.setVisibility(View.VISIBLE);
		else
			addPhotoImageCapture.setVisibility(View.GONE);
	}

	/* 
	 * Method call after edit ads data request showing response........
	 * (non-Javadoc)
	 * @see com.nkaplication.net.DataListener#onDataResponse(boolean, com.nkaplication.net.Response, int, java.lang.Object)
	 */
	@Override
	public void onDataResponse(boolean isError, final Response r,
			final int rid, Object data) {
		h.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (rid == BaseModel.REQUEST_GET_EDIT_ROW_IMAGE_LIST) {
						inflateImagesOnLayout();
					} else if (rid == BaseModel.REQUEST_UPDATE__AD) {
						stopBusy();
						Util.showToastMessage(EditAd.this, getResources()
								.getString(R.string.update_successfully));
						Util.reload = true;
						finish();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * set images on one layout in horizontal mode using layout inflater
	 */

	protected void inflateImagesOnLayout() {
		mobilesImagesLayout.removeAllViews();
		LayoutInflater inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < adsItem.images.size(); i++) {
			// EditAdsMyRowModel imageAds = adsItems.get(i);
			View v = inflate.inflate(R.layout.edit_my_ads_row, null);
			ImageView myeditAdsRowImageIcon = (ImageView) v
					.findViewById(R.id.myeditAdsRowImageIcon);

			ImageView cancelImage = (ImageView) v
					.findViewById(R.id.cancelImage);
			cancelImage.setTag("" + i);
			Bitmap image;
			try {
				//myeditAdsRowImageIcon.setImageBitmap(Util.getBitmapOfImage(
				//		EditAd.this, adsItem.images.get(i)));
				myeditAdsRowImageIcon.setImageBitmap(BitmapFactory.decodeFile(adsItem.images.get(i)));
				// System.out.println(image);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			cancelImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					String tag = view.getTag().toString();
					adsItem.images.remove(Integer.parseInt(tag));
					showMoreImageWithInflate();
				}
			});

			mobilesImagesLayout.addView(v);
		}
	}

	/**
	 * Method for open dialog for pick up image option......... 
	 * @param v
	 */
	public void performAddPhotosCamera(View v) {
		if (adsItem.images.size() < 4)
			Util.showMediaDialogAddPace(this, this);
		else {
			// addPhotoImageCapture.setVisibility(View.GONE);
			Util.showToastMessage(this,
					"You can capture or select only 4 image");
		}
	}

	/**
	 * Method for open gallery for pick up images.......
	 */
	public void openGallery() {

		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);

		startActivityForResult(i, 2);

	}

	/**
	 * Method for open camera to capture images......
	 */
	public void openCamera() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(cameraIntent, 1);

	}

	/* 
	 * Method call after pick up image by gallery or camera..........
	 * (non-Javadoc)
	 * @see com.nkaplication.BaseActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == 1) {
				// imageStoreInSdCard();

				thumbnail = (Bitmap) data.getExtras().get("data");
				File file = Util.getFileStoredCurrentBitmap(thumbnail);
				if (thumbnail != null) {
					adsItem.images.add(file.getAbsolutePath());
					showMoreImageWithInflate();
				}

			}
			if (requestCode == 2) {

				thumbnail = Util.getBitmapOfGellaryImage(EditAd.this, data);
				File file = Util.getFileStoredCurrentBitmap(thumbnail);
				if (thumbnail != null) {
					adsItem.images.add(file.getAbsolutePath());
					showMoreImageWithInflate();
				}

			}

		}
	}

	/**
	 * Method for performing updating ads.........
	 * @param v
	 */
	public void performUpdateAdsAction(View v) {
		String title = editMyAdsTitle.getEditableText().toString();
		String des = editMyAdsDescription.getEditableText().toString();
		String lowPrice, highPrice;
		if (adsItem.status != null && adsItem.status.equalsIgnoreCase(KEY_SELL)) {
			lowPrice = editMyAdsSellPrice.getEditableText().toString();
			adsItem.lowPrice = lowPrice;
		} else {
			lowPrice = editMyAdsBuyLowPrice.getEditableText().toString();
			adsItem.lowPrice = lowPrice;
			if (adsItem.highPrice != null && !adsItem.highPrice.equals("")) {
				highPrice = editMyAdsBuyHighPrice.getEditableText().toString();
				adsItem.highPrice = highPrice;
			}
		}
		if (title != null && !title.trim().equals(""))
			adsItem.title = title;
		if (des != null && !des.trim().equals(""))
			adsItem.description = des;
		if (title != null && !title.trim().equals(""))
			adsItem.title = title;
		
        adsItem.update(EditAd.this, EditAd.this);
	}

	/* 
	 * Method for performing back action.........
	 * (non-Javadoc)
	 * @see com.nkaplication.BaseActivity#goBack(android.view.View)
	 */
	public void goBack(View v) {
		finish();
		if (adsItem.images.size() != imageArraySize)
			Util.reload = true;
	}
}