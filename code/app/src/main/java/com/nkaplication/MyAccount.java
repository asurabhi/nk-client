package com.nkaplication;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.nkaplication.model.BaseModel;
import com.nkaplication.model.EditAdsMyRowModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Util;

/**
 * @author neelam goyal
 * 
 *         Class show all information about user Profile
 * 
 */
public class MyAccount extends BaseActivity implements DataListener {

	private EditText myAccountNameId, myAccountEmailId;
	private EditText myAccountPhoneId;
	private String mobileNumber;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_account);
		ctx = this;
		myAccountNameId = (EditText) findViewById(R.id.myAccountNameId);
		myAccountEmailId = (EditText) findViewById(R.id.myAccountEmailId);
		myAccountPhoneId = (EditText) findViewById(R.id.myAccountPhoneId);
		TextView headerTitle = (TextView) findViewById(R.id.headerTitle);
		headerTitle.setText(getResources().getString(R.string.my_account));

		if (MyPrefs.getInstance().mobileNumber != null
				&& !MyPrefs.getInstance().mobileNumber.equalsIgnoreCase(""))
			myAccountPhoneId.setText(MyPrefs.getInstance().mobileNumber);

		// initActionBarHeader(getResources().getString(R.string.my_account));
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {

		
			
			
			if (Util.isNetworkAvailable(getApplicationContext())) {
				if(mGoogleApiClient!=null)
				{
					showDialogForSignInGoogle(new DialogButtonListener() {
						
						@Override
						public void onButtonClicked(String text) {
							if(text.equals("No")){
								finish();
							}else{
								if (mGoogleApiClient != null){
									startBusy();
									mGoogleApiClient.connect();
								}
							}
							
						}
					});

					
					
				}
				else
				{
				//	setGoogleObject();
				//	mGoogleApiClient.connect();
				}
							} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			setData();
		}
	}

	/**
	 * Method for getting account information from google+ and set data .........
	 */
	public void setAccountGoogleData() {

		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {

			if (Util.isNetworkAvailable(getApplicationContext())) {
				startBusy();
				if (mGoogleApiClient != null
						&& !mGoogleApiClient.isConnecting()) {
					mSignInClicked = true;
					resolveSignInError();
				}
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			setData();
		}

	}

	/**
	 * Method for set name and email data.......
	 */
	protected void setData() {
		myAccountNameId.setText(MyPrefs.getInstance().userFirstName + " "
				+ MyPrefs.getInstance().userLastName);
		myAccountEmailId.setText(MyPrefs.getInstance().email);

	}

	/**
	 * Method for performing update in account information..........
	 * @param v
	 */
	public void performeAccountInfoUpdate(View v) {
		mobileNumber = myAccountPhoneId.getEditableText().toString();
		if (!Util.isNull(mobileNumber)) {
			Util.showToastMessage(MyAccount.this,
					getResources().getString(R.string.enter_mobile_number));
			return;
		}
		EditAdsMyRowModel.updateUserInfo(MyAccount.this, MyAccount.this);
	}

	@Override
	public void onDataResponse(boolean isError, Response r, final int rid,
			Object data) {
		h.post(new Runnable() {

			@Override
			public void run() {
				if (rid == BaseModel.REQUEST_UPDATE_USER_INFO) {
					if (myAccountEmailId.getEditableText().toString() != null
							&& !myAccountEmailId.getEditableText().toString()
									.equals("")) {
						MyPrefs.getInstance().mobileNumber = mobileNumber;
						MyPrefs.getInstance().savePrefs(MyAccount.this);
						Util.showToastMessage(
								MyAccount.this,
								getResources()
										.getString(
												R.string.user_information_update_successfully));
						finish();
					}

				}
			}
		});
	}

}
