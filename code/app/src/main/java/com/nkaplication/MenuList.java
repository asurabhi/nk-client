package com.nkaplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * @author neelam goyal
 * 
 */
public class MenuList extends BaseActivity {
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
	}

	/*
	 * Start an activity of MyAccount
	 */
	public void onClickMyAccountLayout(View v) {
		Intent i = new Intent(MenuList.this, MyAccount.class);
		startActivity(i);
		finish();
	}

}
