package com.nkaplication;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.nkaplication.adp.MyAdsAdapter;
import com.nkaplication.db.NKDB;
import com.nkaplication.model.AdItem;
import com.nkaplication.model.BaseModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Logger;
import com.nkaplication.util.Util;

/**
 * @author Brijesh dutt
 * 
 *         it will be open on click MyAds
 * 
 */
public class MyAdsAndShortList extends BaseActivity implements DataListener {

	private String emailId = "";

	private ArrayList<AdItem> adsItems = new ArrayList<AdItem>();
	private ListView adsList;
	private MyAdsAdapter adapter;
	private ArrayList<String> allAds = new ArrayList<String>();
	private String comeFrom;
	private NKDB db;

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_ads);
		ctx = this;
		// initActionBarHeader(getResources().getString(R.string.my_ads));
		adsList = (ListView) findViewById(R.id.adsList);

		comeFrom = getIntent().getStringExtra("comeFrom");
		TextView headerTitle = (TextView) findViewById(R.id.headerTitle);
		if (comeFrom
				.equalsIgnoreCase(getResources().getString(R.string.my_ads)))
			headerTitle.setText(getResources().getString(R.string.my_ads));
		else if (comeFrom.equalsIgnoreCase(getResources().getString(
				R.string.my_shortlist)))
			headerTitle
					.setText(getResources().getString(R.string.my_shortlist));

		db = new NKDB(MyAdsAndShortList.this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Util.reload) {
			if (MyPrefs.getInstance().email != null
					|| !MyPrefs.getInstance().email.equals("")) {
				Util.reload = false;
				allAds.clear();
				fetchFilesByIdAndLoad();
			}
		}
	}

	/**
	 * Method for fetching all Ads list from db..........
	 */
	public void fetchFilesByIdAndLoad() {
		emailId = MyPrefs.getInstance().email.trim();
		if (comeFrom
				.equalsIgnoreCase(getResources().getString(R.string.my_ads)))
			allAds = db.getMyAdsFilesByEmail(emailId);
		else if (comeFrom.equalsIgnoreCase(getResources().getString(
				R.string.my_shortlist))){
			allAds = db.getAllFiles();
			checkAllAdsMyShortList(allAds);
		}
			
		AdItem.load(this, this);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(getApplicationContext())) {
				
				if(mGoogleApiClient!=null)
				{
					showDialogForSignInGoogle(new DialogButtonListener() {
						
						@Override
						public void onButtonClicked(String text) {
							if(text.equals("No")){
								finish();
							}else{
								if (mGoogleApiClient != null){
									startBusy();
									mGoogleApiClient.connect();
								}
							}
							
						}
					});
					
				}
				else
				{
				//	setGoogleObject();
				//	mGoogleApiClient.connect();
				}
				
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			if (!Util.reload) {
			allAds.clear();
			fetchFilesByIdAndLoad();
			}
		}
	}

	/**
	 * Method for checking google+ login and performing further action......
	 */
	public void setGoogleData() {

		if (MyPrefs.getInstance().email == null
				|| MyPrefs.getInstance().email.equals("")) {
			if (Util.isNetworkAvailable(getApplicationContext())) {
				startBusy();
				if (!mGoogleApiClient.isConnecting()) {
					mSignInClicked = true;
					resolveSignInError();
				}
			} else {
				Util.showToastMessage(getApplicationContext(), getResources()
						.getString(R.string.internet_error));
			}
		} else {
			fetchFilesByIdAndLoad();
		}

	}

	/*
	 * set list in adapter
	 */

	private void setDataIntoListView() {
		Log.d("adsItems list", adsItems.toString());
		if(adsItems.isEmpty()){
			allAds.clear();
			adapter.notifyDataSetChanged();
			return;
		}
		
		boolean checkEditDelete = true;
		adapter = new MyAdsAdapter(MyAdsAndShortList.this, adsItems,checkEditDelete);
		adsList.setAdapter(adapter);
		adsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				NKApplication.getInstance().selectedItem = adsItems
						.get(position);
				String path = db.getPathByAdId(NKApplication.getInstance().selectedItem.adItemId);
				Log.d("Selected item", NKApplication.getInstance().selectedItem.toString());
				Logger.logFile = new File(path);
				//Logger.logFile = new File(allAds.get(position));
				Log.d("All ads lists data", allAds.get(position));
				Log.d("Log file", Logger.logFile.toString());
				Intent intent = new Intent(MyAdsAndShortList.this,
						ItemDetail.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onDataResponse(boolean isError, final Response r,
			final int rid, Object data) {
		h.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (rid == BaseModel.REQUEST_GET_ADS_LIST) {
						// adsItems = JSONParser.parseAdsData(r.obj
						// .getJSONArray("ads"));
						adsItems.clear();
					    Log.d("no of files", allAds.toString());
						for (int i = 0; i < allAds.size(); i++) {

							String data = Logger.readLogFile(
									MyAdsAndShortList.this, allAds.get(i));
							JSONObject obj = new JSONObject(data);
							AdItem allItem = new AdItem();
							allItem.populateFromJson(obj);
							if (comeFrom.equalsIgnoreCase(getResources()
									.getString(R.string.my_shortlist))
									&& allItem.isfavourite && emailId.equals(allItem.email))
								adsItems.add(allItem);
							else if (comeFrom.equalsIgnoreCase(getResources()
									.getString(R.string.my_ads)) && emailId.equals(allItem.email))
								adsItems.add(allItem);
							// adsItems.add(JSONParser.parseAdsData(obj));
							}
						
						setDataIntoListView();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * Start an activity of EditAds
	 */

	public void moveOnEditMyAdsScreen(int position, AdItem item) {
		NKApplication.getInstance().selectedItem = item;
		String path = db.getPathByAdId(NKApplication.getInstance().selectedItem.adItemId);
		Logger.logFile = new File(path);
		//Logger.logFile = new File(allAds.get(position));
		Intent intent = new Intent(MyAdsAndShortList.this, EditAd.class);
		startActivity(intent);
	}

	/**
	 * Method for performing delete ads action..... 
	 * @param position
	 * @param item
	 */
	public void deleteAds(int position, AdItem item) {
		startBusy();
		String path = db.getPathByAdId(item.adItemId);
		db.deleteFileRow(path);
		//db.deleteFileRow(allAds.get(position));
		adsItems.remove(item);
		adapter.notifyDataSetChanged();
		stopBusy();
	}

	/**
	 * Method for check my short list ads data files...........
	 * @param allAdsList
	 */
	public void checkAllAdsMyShortList(ArrayList<String> allAdsList) {
		try {
			ArrayList<String> updatedAdsList = new ArrayList<String>();
			for (int i = 0; i < allAdsList.size(); i++) {

				String data = Logger.readLogFile(MyAdsAndShortList.this,
						allAds.get(i));
				JSONObject obj = new JSONObject(data);
				boolean isLiked = Util.getOptBoolean(obj, "is_favourite");
				if(isLiked){
					updatedAdsList.add(allAdsList.get(i));
				}
             }
			
			allAds.clear();
			allAds.addAll(updatedAdsList);
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	
}
