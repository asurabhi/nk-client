package com.nkaplication.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.nkaplication.R;

public class CustomDialog extends Dialog {
	DialogButtonListener listener;
	String title, message;
	String[] buttonNames;
	TextView titleTextview, messageTextview;
	Button[] buttons = new Button[3];

	public CustomDialog(Context context, String title, String message,
			String[] buttonNames, DialogButtonListener listener) {

		super(context, R.style.MDialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.listener = listener;
		this.title = title;
		this.message = message;
		this.buttonNames = buttonNames;
		this.setCancelable(false);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_dialog);

		buttons[0] = (Button) findViewById(R.id.firstButton);
		buttons[1] = (Button) findViewById(R.id.secondButton);
		buttons[2] = (Button) findViewById(R.id.thirdButton);
		titleTextview = (TextView) findViewById(R.id.titleTextview);
		messageTextview = (TextView) findViewById(R.id.messageTextview);
		titleTextview.setText(title);
		messageTextview.setText(message);
		for (int i = 0; i < buttonNames.length; i++) {
			final int j = i;
			buttons[i].setVisibility(View.VISIBLE);
			buttons[i].setText(buttonNames[i]);
			buttons[i].setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
					if (listener != null)
						listener.onButtonClicked(buttonNames[j]);

				}
			});

		}

	}

}
