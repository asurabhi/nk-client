package com.nkaplication.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;

import org.json.JSONArray;
import org.json.JSONObject;

import com.nkaplication.model.AdItem;

import android.content.Context;
import android.os.Environment;

/**
 * 
 * @author neelam goyal
 * 
 */

public class Logger {

	/**
	 * 
	 * In this method read data from file as string
	 * 
	 * @param filename
	 *            get data based on filename
	 * @return read data as string
	 */
	public static File logFile;

	public static String readAssetsCitiesFile(Context ctx, String filename) {
		String aDataRow, data = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(ctx.getAssets()
					.open(filename)));

			while ((aDataRow = reader.readLine()) != null) {
				data += aDataRow + ",";
			}
		} catch (IOException e) {
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		return data;
	}

	/*
	 * public static String createJsonData(AllAdsListItem item) { JSONObject obj
	 * = new JSONObject(); try {
	 * 
	 * obj.put("title", item.title); obj.put("description", item.description);
	 * obj.put("email", item.email); obj.put("mobile_number", item.mobile);
	 * obj.put("user_name", item.nameUser); obj.put("low_price", item.lowPrice);
	 * obj.put("high_price", item.highPrice); obj.put("status", item.status);
	 * obj.put("condition", item.condition); obj.put("city", item.city);
	 * obj.put("district", item.district); obj.put("category", item.category);
	 * obj.put("sub_category", item.subcategory); obj.put("date_and_time",
	 * item.dateAndTime); obj.put("is_favourite", item.isfavourite);
	 * 
	 * JSONArray company = new JSONArray(); company.put(""); obj.put("images",
	 * company); if(logFile==null) creaFilePath(); createSdJSONFile(); } catch
	 * (Exception e) { e.printStackTrace();
	 * 
	 * } return obj.toString(); }
	 */

	/**
	 * Method to write data in json file..........
	 * @param logmessage
	 */
	public static void writeJSONInFile(String logmessage) {

		try {

			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(logFile, true)));
			out.println(logmessage);
			out.flush();
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method for creating file into SD card..........
	 */
	public static void createSdJSONFile() {

		Writer writer = null;
		try {
			if (logFile.exists())
				logFile.delete();
			logFile.createNewFile();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Method for read log file by file name............
	 * @param ctx
	 * @param filename
	 * @return
	 */
	public static String readLogFile(Context ctx, String filename) {
		String aBuffer = "";
		try {
			File myFile = new File(filename);
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String aDataRow = "";

			while ((aDataRow = myReader.readLine()) != null) {
				aBuffer += aDataRow + "\n";
			}
			myReader.close();
		} catch (Exception e) {
			System.out.println(e);
			return "";
		}
		return aBuffer;
	}

	/**
	 * Method for creating file path......
	 */
	public static void creaFilePath() {
		String fileName = "";
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/NK");
		myDir.mkdirs();
		if (myDir.isDirectory())
			System.out.println("");
		else
			System.out.println("");
		fileName = "nk_json_file_" + System.currentTimeMillis() + ".txt";
		logFile = new File(myDir, fileName);
	}
}
