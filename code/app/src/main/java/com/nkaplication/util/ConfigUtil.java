package com.nkaplication.util;

/**
 * 
 * @author neelam goyal
 * 
 * Class configures the api access
 * 
 */
public class ConfigUtil {


	public static final int MAX_PRICE_RANGE = 100000; 
	// Production configuration
	public static final String PRODUCTION_BASE_URL = "https://www.google.co.in/";


	public static String getBaseURL() {
		
			return PRODUCTION_BASE_URL;
		
	}
	
	public static String getMyAdsListUrl() {
		return getBaseURL();
	}

}
