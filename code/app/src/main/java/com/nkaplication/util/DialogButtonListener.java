package com.nkaplication.util;

public interface DialogButtonListener {

	public void onButtonClicked(String text);
}
