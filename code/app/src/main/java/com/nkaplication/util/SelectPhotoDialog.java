package com.nkaplication.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.nkaplication.R;

public class SelectPhotoDialog extends Dialog {

	DialogButtonListener dl;

	public SelectPhotoDialog(Context ctx, DialogButtonListener dl) {
		super(ctx, R.style.MDialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setCancelable(false);

		this.dl = dl;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		final Button btns[] = new Button[3];
		this.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.select_photo_dialog);

		btns[0] = (Button) findViewById(R.id.pdlgbtn1);
		btns[1] = (Button) findViewById(R.id.pdlgbtn2);
		btns[2] = (Button) findViewById(R.id.pdlgbtn3);

		btns[0].setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (dl != null) {
					dl.onButtonClicked(btns[0].getText().toString());
				}

			}
		});
		btns[1].setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (dl != null) {
					dl.onButtonClicked(btns[1].getText().toString());
				}

			}
		});
		btns[2].setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();

			}
		});

	}

}
