package com.nkaplication.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.nkaplication.EditAd;
import com.nkaplication.NKApplication;
import com.nkaplication.PostAd;
import com.nkaplication.model.SelectPlaceNameModel;
import com.nkaplication.net.Response;
import com.nkaplication.net.ResponseListener;

/**
 * 
 * @author neelam goyal
 * 
 */

public class Util {

	public static boolean reload = false;
	/**
	 * email pattern to check validity of email
	 */
	static final String EMAIL_PATTERN = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

	/**
	 * Network to check Available or not
	 */
	/**
	 * This method is responsible for performing and post based request on
	 * background thread. Once it receive data from server sends response back
	 * on response listener, along with request id
	 * 
	 * @param url
	 *            URL to post data
	 * 
	 * @param data
	 *            actual data to be posed
	 * @param r
	 *            response listener to be called on response or error from
	 *            server
	 * @param rid
	 *            request ID
	 */
	public static void requestAsynData(final String url, final String token,
			final String method, final String data, final ResponseListener r,
			final int rid) {
		Runnable rn = new Runnable() {
			public void run() {
				if (!Util.isNetworkAvailable(NKApplication.getInstance()
						.getApplicationContext())) {
					r.noNetowrk();
					return;
				}
				r.onResponse(getResponse(url, data, token, method), rid);

			}
		};

		new Thread(rn).start();
	}

	/**
	 * Method to check network connection is available or not.......
	 * @param act
	 * @return
	 */
	public static boolean isNetworkAvailable(Context act) {
		ConnectivityManager connectivityManager = (ConnectivityManager) act
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static Response getResponse(String url, String data, String token,
			String method) {
		Response response = null;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) new URL(url).openConnection();
			con.setRequestProperty("Content-Type",
					"application/json; charset=utf-8");
			if (data != null)
				con.setRequestProperty("Content-Length", "" + data.length());
			if (token != null)
				con.setRequestProperty("X-AUTH-TOKEN", token);
			con.setRequestMethod(method);
			// con.setDoInput(true);
			// con.setDoOutput(true);
			con.connect();
			if (data != null) {
				OutputStream out = con.getOutputStream();
				out.write(data.getBytes());
				out.flush();
				out.close();
			}
			int status = 200;// con.getResponseCode();
			if (status == 200) {
				// InputStream in = con.getInputStream();
				if (data == null || data.equals("")) {
					/*
					 * String resp = Logger.readLogFile(NKApplication
					 * .getInstance().getApplicationContext());
					 */
					// String resp = getString(in);
					// Log.e("Response is ", resp);
					// in.close();
					response = new Response("", false);
				} else {

					if (Logger.logFile == null)
						Logger.creaFilePath();
					Logger.createSdJSONFile();

					Logger.writeJSONInFile(data);
					response = new Response(data, false);
				}
			} else if (status == 402 || status == 406) {
				response = new Response("", status);
			} else {
				response = new Response("", true);
			}
		} catch (Exception e) {
			response = new Response("Error", true);
		} finally {
			try {
				con.disconnect();

			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return response;
	}

	/**
	 * Read complete fetched data from server into string
	 */

	private static String getString(InputStream in) {
		StringBuffer r = new StringBuffer();

		try {

			InputStreamReader isr = new InputStreamReader(in, "UTF-8");
			BufferedReader reader = new BufferedReader(isr);
			String line = null;
			while ((line = reader.readLine()) != null)
				r.append(line);
			isr.close();
			reader.close();
			return r.toString();
		} catch (Exception e) {
		}

		return null;
	}

	/**
	 * @param msg
	 *            msg which is show on toast show toast method
	 */

	public static void showToastMessage(Context ctx, String msg) {
		Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * This methos for show error dialog
	 * 
	 * @param title
	 *            title of dialog
	 * @param msg
	 *            error msg which we want to show
	 */
	public static void showErrorDialog(Context ctx, String title, String msg) {
		new CustomDialog(ctx, title, msg, new String[] { "OK" }, null).show();
	}

	/**
	 * Validate email against pattern
	 * 
	 * @param email
	 *            email address to validate
	 * 
	 * @return true if valid email else false
	 */
	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN,
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);

		return matcher.matches();
	}

	/**
	 * Validate text against null and empty
	 * 
	 * @param text
	 *            text is not null and empty
	 * 
	 * @return true if not null and not empty text else false
	 */
	public static boolean isNull(String text) {
		if (text != null && !text.equals(""))
			return true;

		return false;
	}

	/**
	 * key and value is available in any JSON data
	 * 
	 * @param key
	 *            get value based on key
	 * 
	 * @param obj
	 *            obj is json object we get value from this json using key
	 * 
	 * @return if key value is not empty then return value based on key else
	 *         null as string
	 */

	public static String getOptString(JSONObject obj, String key) {
		try {
			return obj.isNull(key) ? "" : obj.getString(key);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * key and value is available in any JSON data
	 * 
	 * @param key
	 *            get value based on key
	 * @param obj
	 *            obj is json object we get value from this json using key
	 * 
	 * @return if key value is not empty then return value based on key else
	 *         false as boolean
	 */
	public static Boolean getOptBoolean(JSONObject obj, String key) {
		try {
			return obj.isNull(key) ? null : obj.getBoolean(key);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	/**
	 * get current date and time
	 * 
	 * @return current date and time
	 */

	public static String getCurrentDateTime() {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		sfd.setTimeZone(TimeZone.getTimeZone("UTC"));
		return sfd.format(new Date());
	}

	/**
	 * Method to open dialog for select images....
	 * @param act
	 * @param ctx
	 */
	public static void showMediaDialogAddPace(final Activity act,
			final Context ctx) {

		SelectPhotoDialog dlg = new SelectPhotoDialog(act,
				new DialogButtonListener() {

					@Override
					public void onButtonClicked(String text) {
						if (text.equals("Camera")) {
							if (ctx instanceof EditAd)
								((EditAd) ctx).openCamera();
							else if (ctx instanceof PostAd)
								((PostAd) ctx).openCamera();

						} else if (text.equals("Gallery")) {
							if (ctx instanceof EditAd)
								((EditAd) ctx).openGallery();
							else if (ctx instanceof PostAd)
								((PostAd) ctx).openGallery();
						}
					}

				});
		dlg.show();

	}

	/**
	 * Method to get bitmap of selected gallery image........
	 * @param act
	 * @param data
	 * @return
	 */
	public static Bitmap getBitmapOfGellaryImage(Activity act, Intent data) {
		Uri selectedImage = data.getData();

		Bitmap bm = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 5;
		AssetFileDescriptor fileDescriptor = null;
		try {
			fileDescriptor = act.getContentResolver().openAssetFileDescriptor(
					selectedImage, "r");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				bm = BitmapFactory.decodeFileDescriptor(
						fileDescriptor.getFileDescriptor(), null, options);
				fileDescriptor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return bm;
	}

	/**
	 * Method for converting DP into pixels........
	 * @param dp
	 * @param context
	 * @return
	 */
	public static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 150f);
		return px;
	}

	/**
	 * Method for changing range format.....
	 * @param range
	 * @return
	 */
	public static String changeRangeFormat(String range) {
		String convertRange = "";
		String[] rangeArr = range.split(" ");

		if (rangeArr.length > 0) {
			int min = Integer.parseInt(rangeArr[0]) / 500;

			int max = Integer.parseInt(rangeArr[2]) / 500;
			convertRange = "" + min + " " + rangeArr[1] + " " + max;
		}

		return convertRange;
	}

	/**
	 * Method for getting bitmap of a image.........
	 * @param context
	 * @param imageUrl
	 * @return
	 */
	public static Bitmap getBitmapOfImage(Context context, String imageUrl) {
		Uri selectedImage = Uri.parse(imageUrl);

		Bitmap bm = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 5;
		AssetFileDescriptor fileDescriptor = null;
		try {
			//ContentResolver mCr=context.getContentResolver();
			//  fileDescriptor=mCr.openAssetFileDescriptor(selectedImage,"r");
			fileDescriptor = context.getContentResolver()
				.openAssetFileDescriptor(selectedImage, "r");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bm = BitmapFactory.decodeFileDescriptor(
						fileDescriptor.getFileDescriptor(), null, options);
				fileDescriptor.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return bm;
	}

	/**
	 * Method for getting locations from list........
	 * @param placesList
	 * @return
	 */
	public static String getLocations(ArrayList<SelectPlaceNameModel> placesList) {
		String locations = "";

		for (int i = 0; i < placesList.size(); i++) {
			if (placesList.get(i).check) {
				locations = locations + placesList.get(i).placeName + ",";
			}
		}

		if (locations.length() > 0
				&& locations.charAt(locations.length() - 1) == ',') {
			locations = locations.substring(0, locations.length() - 1);
		}

		return locations;
	}

	/**
	 * Method for hiding virtual keyboard..........
	 * @param ctx
	 */
	public static void hideSoftKeyboard(Activity ctx) {
		InputMethodManager inputMethodManager = (InputMethodManager) ctx
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(ctx.getCurrentFocus()
				.getWindowToken(), 0);
	}

	/**
	 * Method for getting default city Bangalore........
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static String getCity(double lat, double lon) {
		return "Bangalore";
	}
	
	/**
	 * Method for file stored with current bitmap.......
	 * @param bitmap
	 * @return
	 */
	public static File getFileStoredCurrentBitmap(Bitmap bitmap) {
		FileOutputStream fOut = null;
		File file = null;
		try {
			String file_path = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/NK";
			File dir = new File(file_path);
			if (!dir.exists())
				dir.mkdirs();
			// /File file = null;
			file = new File(dir, "Images"
						+ UUID.randomUUID().toString() + ".png");
			
			fOut = new FileOutputStream(file);

			bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			// fOut.flush();
			// fOut.close();

		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
		return file;
	}
}
