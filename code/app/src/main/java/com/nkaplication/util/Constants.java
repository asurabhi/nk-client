package com.nkaplication.util;

public interface Constants {
	public static final String KEY_ELECTRONICS = "electronics";
	public static final String KEY_AUTO_MOBILES = "auto mobiles";
	public static final String KEY_REAL_ESTATE = "real estate";
	public static final String KEY_HOME_AND_FURNITURE = "home and furniture";
	public static final String KEY_MOBILES = "mobiles";
	public static final String KEY_TABLETS = "tablets";
	public static final String KEY_HOME_APPLIANCES = "home appliances";
	public static final String KEY_ANY_OTHER = "any other";
	public static final String KEY_BUY = "buy";
	public static final String KEY_SELL = "sell";
	public static final String KEY_NEW = "new";
	public static final String KEY_USED = "used";
	
	
	/**
	 * Loding dialog range
	 */

	public static final int DIALOG_LOADING = 1000;
	
	/**
	 * All http methods name
	 */

	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_DELETE = "DELETE";
	public static final String METHOD_PUT = "PUT";
}
