package com.nkaplication.util;

import android.location.Location;

public interface NKLocationListener {
	
	public void onLocation(Location c);

}
