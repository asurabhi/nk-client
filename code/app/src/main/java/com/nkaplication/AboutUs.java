package com.nkaplication;

import android.os.Bundle;
import android.widget.TextView;

/**
 * it will be described about application
 * 
 * @author neelam goyal
 * 
 */
public class AboutUs extends BaseActivity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
	 */
	
	private TextView aboutUsText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us);
		TextView headerTitle = (TextView) findViewById(R.id.headerTitle);
		headerTitle.setText(getResources().getString(R.string.about_us));
		// initActionBarHeader(getResources().getString(R.string.about_us));
		aboutUsText=(TextView)findViewById(R.id.aboutTextId);
		aboutUsText.setText(getResources().getString(R.string.aboutUsText));
		
	}

}
