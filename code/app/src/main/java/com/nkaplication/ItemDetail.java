package com.nkaplication;

import java.util.ArrayList;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;


import com.nkaplication.adp.ViewPagerAdapter;
import com.nkaplication.model.AdItem;
import com.nkaplication.model.BaseModel;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.prefs.MyPrefs;
import com.nkaplication.util.DialogButtonListener;
import com.nkaplication.util.Util;

/**
 * @author Krishan Kumar
 *         <p>
 *         this activity will be described details of mobile
 */
public class ItemDetail extends BaseActivity implements DataListener {
    private TextView mobileName, mobilePrice, mobileStatus, mobileDescription,
            mobileLocation, emailId, mobileNo, mobileDetailCondition;
    private ListView lv;
    private DrawerLayout drawer_layout;
    private ImageView mobileDetailIsFavourite;
    private LinearLayout mobileMobileDetailLayout, emailMobileDetailLayout;
    private Button mobileDetailCallButton;
    private TextView mobileDetailSpaceText;
    private AdItem item;
    private ImageView[] dotsImageView;
    private LinearLayout dotsPanel;
    private int selectedPosition;
    private ViewPager myPager;
    private LinearLayout negotiableLinearLayoutId;

    /*
     * (non-Javadoc)
     *
     * @see com.nkaplication.BaseActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stubss
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_detail);
        ctx = this;
        myPager = (ViewPager) findViewById(R.id.myfivepanelpager);

        dotsPanel = (LinearLayout) findViewById(R.id.dotsPanel);
        /* Set Under Line Below the text */
        menuListButton = (ImageView) findViewById(R.id.mobileDetailMenuIcon);
        lv = (ListView) findViewById(R.id.menuList);
        drawer_layout = (DrawerLayout) findViewById(R.id.mobile_detail_drawer_layout);
        emailId = (TextView) findViewById(R.id.mobileDetailEmailId);
        mobileNo = (TextView) findViewById(R.id.mobileDetailMobileId);
        mobileName = (TextView) findViewById(R.id.mobileDetailMobileNameId);
        mobilePrice = (TextView) findViewById(R.id.mobileDetailPriceId);
        mobileDescription = (TextView) findViewById(R.id.mobileDetailDescriptionId);
        mobileStatus = (TextView) findViewById(R.id.mobileDetailIwantToId);
        mobileLocation = (TextView) findViewById(R.id.mobileDetailLocationId);
        mobileDetailCondition = (TextView) findViewById(R.id.mobileDetailCondition);
        mobileDetailIsFavourite = (ImageView) findViewById(R.id.mobileDetailIsFavourite);
        mobileMobileDetailLayout = (LinearLayout) findViewById(R.id.mobileMobileDetailLayout);
        emailMobileDetailLayout = (LinearLayout) findViewById(R.id.emailMobileDetailLayout);
        mobileDetailCallButton = (Button) findViewById(R.id.mobileDetailCallButton);
        mobileDetailSpaceText = (TextView) findViewById(R.id.mobileDetailSpaceText);
        negotiableLinearLayoutId = (LinearLayout) findViewById(R.id.negotiableDetailLinearLayoutId);
        item = NKApplication.getInstance().selectedItem;
        if (item.isNegotiable)
            negotiableLinearLayoutId.setVisibility(View.VISIBLE);
        else
            negotiableLinearLayoutId.setVisibility(View.GONE);


        if (MyPrefs.getInstance().email == null
                || MyPrefs.getInstance().email.equals("")) {
            mobileDetailIsFavourite
                    .setImageResource(R.drawable.favorite_disable_icon);
        } else {
            if (item.isfavourite)
                mobileDetailIsFavourite
                        .setImageResource(R.drawable.favorite_enable_icon);
            else
                mobileDetailIsFavourite
                        .setImageResource(R.drawable.favorite_disable_icon);
        }

        String price = "";
        if (item.status.equalsIgnoreCase(KEY_BUY))
            price = item.lowPrice + "-" + item.highPrice;
        else
            price = item.lowPrice;
        mobilePrice.setText(price);
        mobileDescription.setText(item.description);
        mobileStatus.setText(item.status);
        mobileLocation.setText(item.city + ", " + item.district);
        mobileDetailCondition.setText(item.condition);
		/*if (item.isEmailVisible) {
			emailMobileDetailLayout.setVisibility(View.VISIBLE);
		}
		if (item.isMobileVisible) {
			mobileDetailCallButton.setVisibility(View.VISIBLE);
			mobileMobileDetailLayout.setVisibility(View.VISIBLE);
			mobileDetailSpaceText.setVisibility(View.VISIBLE);
		}
		*/


        if (!item.isEmailVisible) {
            emailMobileDetailLayout.setVisibility(View.VISIBLE);
        }

        if (item.isMobileVisible) {
            mobileDetailSpaceText.setVisibility(View.VISIBLE);
        } else {
            mobileDetailCallButton.setVisibility(View.VISIBLE);
            mobileMobileDetailLayout.setVisibility(View.VISIBLE);
        }


        emailId.setText(item.email);
        mobileNo.setText(item.mobile);

        mobileName.setText(Html.fromHtml("<b><u>" + item.title + "</u></b>"));

        setHeaderWithSlider();

        if (item != null && item.images != null && item.images.size() > 0) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(this, item.images);
            myPager.setAdapter(adapter);
            myPager.setCurrentItem(0);
            myPager.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    selectDot(position);
                }

                @Override
                public void onPageScrolled(int position, float arg1, int arg2) {
                    selectedPosition = position;

                    selectDot(position);
                }

                @Override
                public void onPageScrollStateChanged(int position) {
                    if (position == 2) {
                        selectDot(selectedPosition);
                    }
                }
            });
        } else {
            myPager.setVisibility(View.GONE);
        }

        if (item.images.size() > 1) {
            createDotPanel();
            selectDot(0);
        }
    }

    /**
     * method of menu slider
     */

    public void setHeaderWithSlider() {

        initActionBar(drawer_layout, lv);
        drawer_layout.setDrawerListener(this);
    }

    /**
     * Method for performing sending message action........
     *
     * @param v
     */
    public void performActionOnSendMessage(View v) {

        sendMessageDialog();


    }

    @Override
    public void onDataResponse(boolean isError, Response r, final int rid,
                               Object data) {
        h.post(new Runnable() {

            @Override
            public void run() {
                try {
                    if (rid == BaseModel.REQUEST_SEND_MESSAGE) {

                        Util.showToastMessage(ItemDetail.this, getResources()
                                .getString(R.string.message_send_successfully));
                    }
                    if (rid == BaseModel.REQUEST_UPDATE__AD) {
                        stopBusy();
                        Util.reload = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method for performing telephonic call action........
     *
     * @param v
     */
    public void performActionCall(View v) {
        Uri number = Uri.parse("tel:" + item.mobile);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    /**
     * Method for performing like or unlike action.........
     *
     * @param v
     */
    public void performActionIsLiked(View v) {

        if (MyPrefs.getInstance().email == null
                || MyPrefs.getInstance().email.equals("")) {
            if (Util.isNetworkAvailable(getApplicationContext())) {

                if (mGoogleApiClient != null) {
                    checkForSendButton = false;
                    showDialogForSignInGoogle(new DialogButtonListener() {

                        @Override
                        public void onButtonClicked(String text) {
                            if (text.equals("No")) {
                                //finish();
                            } else {
                                if (mGoogleApiClient != null) {
                                    startBusy();
                                    mGoogleApiClient.connect();
                                }
                            }

                        }
                    });

                } else {
                    setGoogleObject();
                    mGoogleApiClient.connect();
                }
			
				/*setGoogleObject();
				if (mGoogleApiClient != null
						&& !mGoogleApiClient.isConnecting()) {
					startBusy();
					mSignInClicked = true;
					resolveSignInError();
				}*/
            } else {
                Util.showToastMessage(getApplicationContext(), getResources()
                        .getString(R.string.internet_error));
            }
        } else {

            if (item.email.equals(MyPrefs.getInstance().email)) {
                if (item.isfavourite) {
                    mobileDetailIsFavourite
                            .setImageResource(R.drawable.favorite_disable_icon);
                    item.isfavourite = false;
                } else {
                    mobileDetailIsFavourite
                            .setImageResource(R.drawable.favorite_enable_icon);
                    item.isfavourite = true;
                }
                item.update(ItemDetail.this, ItemDetail.this);
            } else {
                Util.showToastMessage(ItemDetail.this, getResources().getString(R.string.non_user_like_unlike_text));
            }

        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if (MyPrefs.getInstance().email == null
                || MyPrefs.getInstance().email.equals("")) {
            if (Util.isNetworkAvailable(getApplicationContext())) {
                //	showDialogForSignInGoogle();
            } else {
                Util.showToastMessage(getApplicationContext(), getResources()
                        .getString(R.string.internet_error));
            }
        }
    }

    /**
     * Method for creating dots for no of images...........
     */
    private void createDotPanel() {
        ArrayList<String> images = item.images;
        if (images == null || images.size() == 0)
            return;

        dotsImageView = new ImageView[images.size()];

        for (int i = 0; i < images.size(); i++) {
            dotsImageView[i] = new ImageView(this);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            if (i > 0) {
                params.leftMargin = 10;
            }
            params.gravity = Gravity.CENTER;
            dotsImageView[i].setLayoutParams(params);
            dotsImageView[i].setAdjustViewBounds(true);
            dotsImageView[i].setScaleType(ScaleType.FIT_CENTER);
            dotsImageView[i].setImageResource(R.drawable.disable);
            dotsPanel.addView(dotsImageView[i]);
        }
    }

    /**
     * Method for dot selecting position........
     *
     * @param position
     */
    private void selectDot(int position) {
        if (dotsImageView == null || dotsImageView.length == 0) {
            return;
        }

        for (int i = 0; i < dotsImageView.length; i++) {
            if (i == position) {
                dotsImageView[i].setImageResource(R.drawable.enable);

            } else {
                dotsImageView[i].setImageResource(R.drawable.disable);
            }

        }
    }


    /**
     * Method for showing dialog when click on message button..........
     */
    private void sendMessageDialog() {

        final Dialog dlg = new Dialog(ItemDetail.this, R.style.FullHeightDialog);
        dlg.setContentView(R.layout.send_message_dialog);
        dlg.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dlg.getWindow().setGravity(Gravity.CENTER);
        // adding text dynamically
        final EditText messageEditText = (EditText) dlg
                .findViewById(R.id.messageEditText);
        Button sendButton = (Button) dlg
                .findViewById(R.id.sendButton);
        Button cancelButton = (Button) dlg
                .findViewById(R.id.cancelButton);


        sendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyPrefs.getInstance().email == null
                        || MyPrefs.getInstance().email.equals("")) {
                    if (Util.isNetworkAvailable(getApplicationContext())) {


                        if (mGoogleApiClient != null) {
                            dlg.dismiss();
                            showDialogForSignInGoogle(new DialogButtonListener() {

                                @Override
                                public void onButtonClicked(String text) {
                                    if (text.equals("No")) {
                                        //finish();
                                    } else {
                                        if (mGoogleApiClient != null) {
                                            startBusy();
                                            mGoogleApiClient.connect();
                                        }
                                    }

                                }
                            });
                        } else {
                            setGoogleObject();
                            mGoogleApiClient.connect();
                        }


                    } else {
                        Util.showToastMessage(getApplicationContext(), getResources()
                                .getString(R.string.internet_error));
                    }
                } else {
                    String message = messageEditText.getEditableText().toString();
                    if (message.equalsIgnoreCase("")) {
                        Util.showToastMessage(ItemDetail.this, "Please enter text...");
                    } else {
                        dlg.dismiss();
                        item.sendMessage(ItemDetail.this, ItemDetail.this);
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });

        dlg.show();
    }

    /**
     * Method for click on pager ............
     *
     * @param position
     */
    public void pagerClickAction(int position) {
        Intent in = new Intent(ItemDetail.this, FullScreenImage.class);
        in.putExtra("position", position);
        startActivity(in);
    }


    public void setItemDetailAdGoogleData() {

        if (MyPrefs.getInstance().email == null
                || MyPrefs.getInstance().email.equals("")) {

            if (Util.isNetworkAvailable(getApplicationContext())) {
                startBusy();
                if (mGoogleApiClient != null
                        && !mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    resolveSignInError();
                }
            } else {
                Util.showToastMessage(getApplicationContext(), getResources()
                        .getString(R.string.internet_error));
            }
        } else {

            if (item.email.equals(MyPrefs.getInstance().email)) {
                if (item.isfavourite) {
                    mobileDetailIsFavourite
                            .setImageResource(R.drawable.favorite_disable_icon);
                    item.isfavourite = false;
                } else {
                    mobileDetailIsFavourite
                            .setImageResource(R.drawable.favorite_enable_icon);
                    item.isfavourite = true;
                }
                item.update(ItemDetail.this, ItemDetail.this);
            } else {
                Util.showToastMessage(ItemDetail.this, getResources().getString(R.string.non_user_like_unlike_text));
            }

        }

    }


}
