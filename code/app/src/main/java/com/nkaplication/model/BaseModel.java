package com.nkaplication.model;

import org.json.JSONObject;

/**
 * @author neelam goyal
 *
 *Common model class which extends into all model classes
 */
public abstract class BaseModel {

	public static int REQUEST_GET_ADS_LIST = 0;
	public static int REQUEST_GET_ELECTRONICS_MOBILES_LIST = 1;
	public static int REQUEST_GET_EDIT_ROW_IMAGE_LIST = 2;
	public static int REQUEST_GET_ALL_ADS_LIST = 3;
	public static int REQUEST_UPDATE__AD = 4;
	public static int REQUEST_SEND_MESSAGE = 5;
	public static int REQUEST_UPDATE_USER_INFO = 6;
	
	public abstract String toJson();
	public abstract void populateFromJson(JSONObject doc);

}
