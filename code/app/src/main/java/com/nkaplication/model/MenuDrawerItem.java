package com.nkaplication.model;


/**
 * 
 * Model class to represent menu list 
 * 
 * @author neelam goyal
 * 
 */


public class MenuDrawerItem {
	
	private String title;
	private int icon;
	
	public MenuDrawerItem(){}

	/**
	 * 
	 * @param title
	 * 			menu list title
	 * @param icon
	 * 			icon is image of menu list 
	 * 
	 * 
	 */
	public MenuDrawerItem(String title , int icon){
		this.title = title;
		this.icon = icon;
	}
	public String getTitle(){
		return this.title;
	}
	
	public int getIcon(){
		return this.icon;
	}
	
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setIcon(int icon){
		this.icon = icon;
	}
	
	
}
