package com.nkaplication.model;

import org.json.JSONObject;

import android.graphics.Bitmap;

import com.nkaplication.BaseActivity;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.net.ResponseListener;
import com.nkaplication.util.ConfigUtil;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Util;

/**
 * 
 * Model class to represent a edit ads
 * 
 * @author neelam goyal
 * 
 */
public class EditAdsMyRowModel extends BaseModel implements Constants {

	// public String imageIcon;
	public Bitmap caption;

	/**
	 * 
	 * load complete data from server of edit ads
	 * 
	 * @param dl
	 *            dl is response listener object
	 * 
	 */

	public static void load(final BaseActivity a, final DataListener dl) {
		a.startBusy();
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_GET,
				null, new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {

						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_GET_EDIT_ROW_IMAGE_LIST);

	}

	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void populateFromJson(JSONObject doc) {
		// TODO Auto-generated method stub

	}

	public static void updateUserInfo(final BaseActivity a,
			final DataListener dl) {
		a.startBusy();
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_PUT,
				null, new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {
						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_UPDATE_USER_INFO);

	}

}
