package com.nkaplication.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.nkaplication.BaseActivity;
import com.nkaplication.net.DataListener;
import com.nkaplication.net.Response;
import com.nkaplication.net.ResponseListener;
import com.nkaplication.util.ConfigUtil;
import com.nkaplication.util.Constants;
import com.nkaplication.util.Util;

/**
 * 
 * Model class to represent mobile list
 * 
 * @author neelam goyal
 * 
 */
public class AdItem extends BaseModel implements Constants {

	public int adItemId;
	public String title;
	public String lowPrice;
	public String highPrice;
	public String description;
	public String city;
	public String district;
	public String condition;
	public String category;
	public String subcategory;
	public String status;
	public String email;
	public String mobile;
	public String nameUser;
	public ArrayList<String> images = new ArrayList<String>();
	public String dateAndTime;
	public boolean isfavourite;
	public boolean isMobileVisible;
	public boolean isEmailVisible;
	public boolean isNegotiable;

	public void storeData(final BaseActivity a, final DataListener dl) {
		a.startBusy();
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_POST,
				toJson(), new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {
						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_GET_ALL_ADS_LIST);

	}

	@Override
	public String toJson() {

		JSONObject obj = new JSONObject();
		try {
			obj.put("adItemId", adItemId);
			obj.put("title", title);
			obj.put("description", description);
			obj.put("email", email);
			obj.put("mobile_number", mobile);
			obj.put("user_name", nameUser);
			obj.put("low_price", lowPrice);
			obj.put("high_price", highPrice);
			obj.put("status", status);
			obj.put("condition", condition);
			obj.put("city", city);
			obj.put("district", district);
			obj.put("category", category);
			obj.put("sub_category", subcategory);
			obj.put("date_and_time", dateAndTime);
			obj.put("is_favourite", isfavourite);
			obj.put("is_mobile_visible", isMobileVisible);
			obj.put("is_email_visible", isEmailVisible);
			obj.put("is_Negotiable", isNegotiable);

			JSONArray company = new JSONArray();
			for (int i = 0; i < images.size(); i++) {
				company.put(images.get(i));
			}
			obj.put("images", company);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return obj.toString();

	}

	@Override
	public void populateFromJson(JSONObject arrayObj) {

		try {
			adItemId = arrayObj.getInt("adItemId");
			title = Util.getOptString(arrayObj, "title");
			condition = Util.getOptString(arrayObj, "condition");
			city = Util.getOptString(arrayObj, "city");
			district = Util.getOptString(arrayObj, "district");
			lowPrice = Util.getOptString(arrayObj, "low_price");
			highPrice = Util.getOptString(arrayObj, "high_price");
			status = Util.getOptString(arrayObj, "status");
			category = Util.getOptString(arrayObj, "category");
			dateAndTime = Util.getOptString(arrayObj, "date_and_time");
			description = Util.getOptString(arrayObj, "description");
			email = Util.getOptString(arrayObj, "email");
			mobile = Util.getOptString(arrayObj, "mobile_number");
			nameUser = Util.getOptString(arrayObj, "user_name");
			subcategory = Util.getOptString(arrayObj, "sub_category");
			isfavourite = Util.getOptBoolean(arrayObj, "is_favourite");
			isMobileVisible = Util.getOptBoolean(arrayObj, "is_mobile_visible");
			isEmailVisible = Util.getOptBoolean(arrayObj, "is_email_visible");
			isNegotiable = Util.getOptBoolean(arrayObj, "is_Negotiable");
			JSONArray array = arrayObj.getJSONArray("images");
			for (int i = 0; i < array.length(); i++) {
				/*
				 * EditAdsMyRowModel imageIcon = new EditAdsMyRowModel();
				 * imageIcon.imageIcon = array.getString(i);
				 * item.images.add(imageIcon);
				 */images.add(array.getString(i));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void update(final BaseActivity a, final DataListener dl) {

		a.startBusy();
		Log.d("itemData", toJson());
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_PUT,
				toJson(), new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						//a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {
						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_UPDATE__AD);

	}

	public void sendMessage(final BaseActivity a, final DataListener dl) {

		a.startBusy();
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_POST,
				null, new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {
						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_SEND_MESSAGE);

	}

	public static void load(final BaseActivity a, final DataListener dl) {

		a.startBusy();
		Util.requestAsynData(ConfigUtil.getMyAdsListUrl(), "", METHOD_GET,
				null, new ResponseListener() {

					@Override
					public boolean onResponse(final Response r, final int rid) {
						a.stopBusy();
						dl.onDataResponse(false, r, rid, null);
						return false;
					}

					@Override
					public void noNetowrk() {
						a.stopBusy();
						a.showNetworkError();
					}
				}, REQUEST_GET_ADS_LIST);

	}

}
